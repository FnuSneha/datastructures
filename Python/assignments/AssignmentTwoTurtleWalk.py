#Using the example of Turtle Graphics in my book,
#Python By Example, create a set of
#functions that will enable the turtle to draw
#some simple pictures and diagrams.

from turtle import *
color('blue', 'green')
begin_fill()
while True:
    forward(200)
    left(170)
    if abs(pos()) < 1:
        break
end_fill()
color('red', 'yellow')
begin_fill()
while True:
    forward(200)
    left(170)
    if abs(pos()) < 1:
        break
end_fill()
color('black', 'purple')
begin_fill()
while True:
    forward(200)
    left(170)
    if abs(pos()) < 1:
        break
end_fill()
done()
