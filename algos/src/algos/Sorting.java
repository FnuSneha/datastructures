package algos;

import java.util.ArrayList;
import java.util.List;

public class Sorting {
	public static void quickSort(int[] arr) {
		int low = 0;
		int high = arr.length - 1;
		quickSortH(arr, low, high);

	}

	public static void quickSortH(int[] arr, int low, int high) {
		if (arr == null || arr.length == 0)
			return;
		if (low >= high) {
			return;
		}
		int middle = low + (high - low) / 2;
		int pivot = arr[middle];
		int i = low;
		int j = high;
		while (low <= high) {
			while (arr[low] < pivot) {
				i++;
			}
			while (arr[high] > pivot) {
				j--;
			}
			if (i <= j) {
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
				i++;
				j--;

			}
		}

	}

	public static void mergeSort(int[] arr) {
		if (arr.length < 2) {
			return;
		}
		int size = arr.length;
		int middle = size / 2;
		int leftSize = middle;
		int rightSize = size - middle;
		int[] left = new int[leftSize];
		int[] right = new int[rightSize];
		for (int i = 0; i < middle; i++) {
			left[i] = arr[i];
		}
		for (int i = middle; i < size; i++) {
			right[i - middle] = arr[i];
		}
		mergeSort(left);
		mergeSort(right);
		mergeTwoSortedArraysMergeSort(left, right, arr);

	}

	private static void mergeTwoSortedArraysMergeSort(int arr1[], int arr2[],
			int[] result) {
		int i = 0, j = 0, k = 0;
		for (; i < arr1.length && j < arr2.length;) {
			if (arr1[i] <= arr2[j]) {
				result[k] = arr1[i];
				i++;
				k++;
			} else {
				result[k] = arr2[j];
				j++;
				k++;
			}
		}
		if (i == arr1.length) {
			while (j < arr2.length) {
				result[k] = arr2[j];
				j++;
				k++;
			}
		} else {
			while (i < arr1.length) {
				result[k] = arr1[i];
				i++;
			}
		}
	}

	public static List<Integer> mergeTwoSortedArrays(int arr1[], int arr2[]) {
		List<Integer> resultList = new ArrayList<Integer>();
		int i = 0, j = 0;
		for (; i < arr1.length && j < arr2.length;) {
			if (arr1[i] <= arr2[j]) {
				resultList.add(arr1[i]);
				i++;
			} else {
				resultList.add(arr2[j]);
				j++;
			}
		}
		if (i == arr1.length) {
			while (j < arr2.length) {
				resultList.add(arr2[j]);
				j++;
			}
		} else {
			while (i < arr1.length) {
				resultList.add(arr1[i]);
				i++;
			}
		}

		return resultList;

	}

	public static void insertionSort(int[] arr) {
		if (arr.length == 0 || arr == null) {
			return;
		}
		for (int i = 1; i < arr.length; i++) {
			int item_to_be_inserted = arr[i];
			int j = i - 1;
			while (j >= 0 && arr[j] > item_to_be_inserted) {
				arr[j + 1] = arr[j];
				j--;
			}
			arr[j + 1] = item_to_be_inserted;
		}
	}


}
