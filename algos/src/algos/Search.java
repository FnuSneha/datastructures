package algos;

public class Search {
	public static int binarySearchRecursion(int[] arr, int key) {
		return binarySearchRecursionHelper(arr, key, 0, arr.length - 1);
	}

	public static int binarySearchRecursionHelper(int[] arr, int key, int low,
			int high) {
		if (low > high) {
			return -1;
		}
		int mid = (low + high) / 2;
		if (arr[mid] == key) {
			return mid;
		} else if (arr[mid] > key) {
			return binarySearchRecursionHelper(arr, key, low, mid-1);
		} else {
			return binarySearchRecursionHelper(arr, key, mid+1, high);
		}

	}

	public static int binarySearchIteration(int[] arr, int key) {
		int high = arr.length - 1;
		int low = 0;

		while (low <= high) {
			int mid = high + low / 2;
			if (arr[mid] == key) {
				return mid;
			}
			if (key < arr[mid]) {
				high = mid - 1;
			} else {
				low = mid + 1;
			}

		}
		return -1;
	}

}
