package algos;

public class Fibonicci {
	public static int findFibIteration(int num) {
		int x = 0;
		int y = 1;
		int z = 1;
		for (int i = 0; i < num - 1; i++) {
			x = y;
			y = z;
			z = x + y;
		}
		return x;

	}

	public static int findFibRecursion(int num) {
		if (num == 0 || num == 1) {
			return num;
		}

		return findFibRecursion(num - 1) + findFibRecursion(num - 2);

	}

	public static void findFibMatrix(int num) throws MatrixExceptions {
		int[][] ourArray = { { 1, 1 }, { 1, 0 } };

		int product[][] = { { 1, 0 }, { 0, 1 } };//This is identity mtrix
		for (; num > 1; num--) {
			product = Matrix.multiplyMatrixCrossProduct(product, ourArray);

		}
		Matrix.printMatrix(product);

	}
}
