package algos;

public class PowerNumber {
	public static int powerNum(int num, int exp) {
		if (exp == 0) {
			return 1;
		}
		int product = 1;
		while (exp > 0) {
			product = product * num;
			exp--;

		}
		return product;

	}

	public static int powerNumRecursion(int num, int exp) {
		if (exp == 0) {
			return 1;
		}
		if (exp % 2 == 0) {
			return powerNumRecursion(num, exp / 2)
					* powerNumRecursion(num, exp / 2);
		} else {
			return powerNumRecursion(num, (exp - 1) / 2)
					* powerNumRecursion(num, (exp - 1) / 2) * num;
		}
	}
}
