package algos;

public class Matrix {

	public static int[][] multiplyMatrixRecursion(int[][] a, int[][] b) {
		int aRow = a.length;
		int aCol = a.length;
		int bRow = a.length;
		int bCol = a.length;
		int n = a.length;
		return multiplyMatrixRecursionHelper(a, b, aRow, aCol, bRow, bCol, n);

	}

	private static int[][] multiplyMatrixRecursionHelper(int[][] a, int[][] b,
			int aRow, int aCol, int bRow, int bCol, int n) {
		int[][] c = new int[n][n];
		if (n == 1) {
			c[0][0] = a[aRow][aCol] * b[bRow][bCol];
		}
		setSubMatrix(
				0,
				0,
				n / 2,
				n / 2,
				multiplyMatrixRecursionHelper(a, b, aRow, aCol, bRow, bCol,
						n / 2),
				multiplyMatrixRecursionHelper(a, b, aRow, aCol + n / 2, bRow
						+ n / 2, bCol, n / 2), c);
		setSubMatrix(
				0,
				n / 2,
				n / 2,
				n,
				multiplyMatrixRecursionHelper(a, b, aRow, aCol, bRow, bCol
						+ (n / 2), n / 2),
				multiplyMatrixRecursionHelper(a, b, aRow, aCol + (n / 2), bRow
						+ (n / 2), bCol + (n / 2), n / 2), c);
		setSubMatrix(
				n / 2,
				0,
				n,
				n / 2,
				multiplyMatrixRecursionHelper(a, b, aRow, aCol, bRow, bCol,
						n / 2),
				multiplyMatrixRecursionHelper(a, b, aRow, aCol + n / 2, bRow
						+ n / 2, bCol, n / 2), c);
		setSubMatrix(
				n / 2,
				n / 2,
				n,
				n,
				multiplyMatrixRecursionHelper(a, b, aRow + (n / 2), aCol, bRow,
						bCol + (n / 2), n / 2),
				multiplyMatrixRecursionHelper(a, b, aRow + (n / 2), aCol
						+ (n / 2), bRow + (n / 2), bCol + (n / 2), n / 2), c);

		return c;

	}

	private static void setSubMatrix(int cRow, int cCol, int cRown, int cColn,
			int[][] a, int[][] b, int[][] c) {
		for (int i = 0; i < cRown; i++) {
			for (int j = 0; j < cColn; j++) {
				c[i][j] = a[i - cRow][j - cCol] + b[i - cRow][j - cCol]; // why
																			// doing
																			// [i-cRow][j-cCol],dnt
																			// know
																			// yet
			}
		}

	}

	public static int[][] addMatrix(int[][] arr1, int[][] arr2) {
		int[][] arr3 = new int[arr1.length][];
		printMatrix(arr1);
		System.out.println(" + ");
		printMatrix(arr2);
		System.out.println(" = ");
		for (int i = 0; i < arr1.length; i++) {
			arr3[i] = new int[arr1[i].length];
			for (int j = 0; j < arr1[i].length; j++) {
				arr3[i][j] = arr1[i][j] + arr2[i][j];
			}
		}
		return arr3;

	}

	public static int[][] multiplyMatrixDotProduct(int[][] arr1, int[][] arr2) {
		int[][] arr3 = new int[arr1.length][];
		printMatrix(arr1);
		System.out.println(" + ");
		printMatrix(arr2);
		System.out.println(" = ");
		for (int i = 0; i < arr1.length; i++) {
			arr3[i] = new int[arr1[i].length];
			for (int j = 0; j < arr1[i].length; j++) {
				arr3[i][j] = arr1[i][j] * arr2[i][j];
			}
		}
		return arr3;
	}

	public static int[][] multiplyMatrixCrossProduct(int[][] arr1, int[][] arr2)
			throws MatrixExceptions {
		printMatrix(arr1);
		System.out.println(" * ");
		printMatrix(arr2);
		System.out.println(" = ");
		int arr1Row = arr1.length;
		int arr1Col = arr1[0].length;
		int arr2Row = arr2.length;
		int arr2Col = arr2[0].length;
		if (arr1Col != arr2Row) {
			throw new MatrixExceptions();
		}
		int[][] arr3 = new int[arr1Row][arr2Col];
		for (int i = 0; i < arr1Row; i++) {
			for (int j = 0; j < arr2Col; j++) {
				arr3[i][j] = 0;
				for (int k = 0; k < arr1Col; k++) {
					arr3[i][j] += arr1[i][k] * arr2[k][j];
				}
			}
		}
		return arr3;

	}

	public static void inverseMatrix(int[][] arr1, int[][] arr2) {

	}

	public static void printMatrix(int[][] arr) {
		System.out.println("Printing your Matrix");
		System.out.println("Number of Rows in your matrix is: " + arr.length);
		for (int i = 0; i < arr.length; i++) {
			System.out.println("Number of elements in your " + i + " row is: "
					+ arr[i].length);

			System.out.print("elements are: ");
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " , ");

			}
			System.out.println();

		}

	}
}
