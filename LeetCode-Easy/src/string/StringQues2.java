package string;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class StringQues2 {
	
	//not accepting on leetcode for some pecial edge cases
	public static String shortestPalindrome214(String s) {
		if (s == null || s.length() <= 1) {
			return s;
		}
		int mid = s.length() / 2;
		for (int i = mid; i >= 1; i--) {
			if (s.charAt(i) == s.charAt(i - 1)) {
				if (shortestPalindrome214EvenSlotHelper(s, i)) {
					StringBuilder builder = new StringBuilder();
					builder.append(s.substring(i));
					builder.reverse();
					builder.append(s.substring(i));
					return builder.toString();
				}
			} else {
				if (shortestPalindrome214OddSlotHelper(s, i)) {
					System.out.println(i);
					StringBuilder builder = new StringBuilder();
					builder.append(s.substring(i));
					builder.reverse();
					builder.append(s.substring(i - 1));
					return builder.toString();
				}
			}
		}
		return null;

	}

	private static boolean shortestPalindrome214EvenSlotHelper(String s, int m) {
		int j = 1;
		while ((m - 1) - j >= 0) {
			if (s.charAt(m + j) != s.charAt((m - 1) - j)) {
				return false;
			}
			j++;
		}
		return true;

	}
	private static boolean shortestPalindrome214OddSlotHelper(String s, int m) {
		int j = 0;
		while ((m - 2) - j >= 0) {
			if (s.charAt((m) + j) != s.charAt((m - 2) - j)) {
				return false;
			}
			j++;
		}

		return true;

	}

	// runtime is too much foor leetcode to accept for this.
	/*
	 * public static String shortestPalindrome214(String s) { if
	 * (isPalindrome125(s)) { return s; } int i; for (i = s.length(); i >= 0;
	 * i--) { if (isPalindrome125(shortestPalindrome214Helper(s, i))) { break; }
	 * 
	 * } return shortestPalindrome214Helper(s, i);
	 * 
	 * }
	 */
	private static String shortestPalindrome214Helper(String s, int n) {
		if (n == 0) {
			return s;
		}
		StringBuilder builder = new StringBuilder();
		for (int i = s.length() - 1; i >= n; i--) {
			builder.append(s.charAt(i));
		}
		builder.append(s);
		return builder.toString();
	}

	// accepted solution on leetcode
	public static String countAndSay38(int n) {
		if (n == 0) {
			return null;
		}
		String s = "1";
		for (int i = 0; i < n - 1; i++) {
			s = countAndSay38Helper(s);
		}
		return s;

	}

	private static String countAndSay38Helper(String s) {
		StringBuilder builder = new StringBuilder();
		char prev = '\0';
		int count = 0;
		for (int i = 0; i < s.length(); i++) {
			char curr = s.charAt(i);
			if (curr == prev) {
				count++;
			} else {
				if (count != 0) {
					builder.append(count);
					builder.append(prev);
				}
				count = 1;
			}
			prev = curr;
		}
		if (count != 0) {
			builder.append(count);
			builder.append(prev);
		}

		return builder.toString();

	}

	// accepted solution. weird but \0 cant be used.
	public static int lengthOfLastWord58(String s) {
		if (s == null || s.length() == 0)
			return 0;

		int result = 0;
		int len = s.length();

		boolean flag = false;
		for (int i = len - 1; i >= 0; i--) {
			char c = s.charAt(i);
			if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
				flag = true;
				result++;
			} else {
				if (flag)
					return result;
			}
		}

		return result;

	}

	// This is right but not accepted on leetcode due to time exceeded
	public static List<List<Integer>> palindromePairs336(String[] words) {
		if (words.length == 0 || words == null) {
			return null;
		}

		List<List<Integer>> ans = new ArrayList<List<Integer>>();
		Map<Character, ArrayList<Integer>> firstMap = new HashMap<Character, ArrayList<Integer>>();
		Map<Character, ArrayList<Integer>> secondMap = new HashMap<Character, ArrayList<Integer>>();
		for (int i = 0; i < words.length; i++) {
			if (words[i] == null) {
				return null;
			}
			if (words[i].length() == 0) {
				if (firstMap.containsKey('\0')) {
					firstMap.get('\0').add(i);
				} else {
					ArrayList list = new ArrayList();
					list.add(i);
					firstMap.put('\0', list);
				}
			} else {
				if (firstMap.containsKey(words[i].charAt(0))) {
					firstMap.get(words[i].charAt(0)).add(i);
				} else {
					ArrayList list = new ArrayList();
					list.add(i);
					firstMap.put(words[i].charAt(0), list);
				}
				if (secondMap
						.containsKey(words[i].charAt(words[i].length() - 1))) {
					secondMap.get(words[i].charAt(words[i].length() - 1))
							.add(i);
				} else {
					ArrayList list = new ArrayList();
					list.add(i);
					secondMap.put(words[i].charAt(words[i].length() - 1), list);
				}
			}

		}
		for (int i = 0; i < words.length; i++) {
			boolean isPalin = isPalindrome125(words[i]);
			if (isPalin) {
				List<Integer> list = firstMap.get('\0');
				if (list != null) {
					for (int j : list) {
						if (i != j) {
							List<Integer> indexList = new ArrayList<Integer>();
							indexList.add(i);
							indexList.add(j);
							ans.add(indexList);
							List<Integer> indexList2 = new ArrayList<Integer>();
							indexList2.add(j);
							indexList2.add(i);
							ans.add(indexList2);
						}
					}
				}

			}

			if (words[i].length() != 0) {
				if (secondMap.containsKey(words[i].charAt(0))) {
					List<Integer> list = secondMap.get(words[i].charAt(0));
					for (int j : list) {
						if (i != j) {
							StringBuilder builder = new StringBuilder();
							builder.append(words[i]);
							builder.append(words[j]);
							if (isPalindrome125(builder.toString())) {
								List<Integer> indexList = new ArrayList<Integer>();
								indexList.add(i);
								indexList.add(j);
								ans.add(indexList);

							}

						}
					}

				}
			}

		}
		return ans;
	}

	// leetcode accepted solution
	public static boolean isPalindrome125(String s) {
		if (s == null || s.length() == 0) {
			return true;
		}

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			if (isAplhaNumeric(s.charAt(i))) {
				builder.append(Character.toLowerCase(s.charAt(i)));
			}
		}
		String str = builder.toString();
		for (int i = 0; i < str.length() / 2; i++) {
			if (str.charAt(i) != str.charAt((str.length() - 1) - i)) {
				return false;
			}
		}
		return true;
	}

	private static boolean isAplhaNumeric(char ch) {
		if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch >= '0'
				&& ch <= '9') {
			return true;
		}
		return false;
	}
	//ccepted solution on leetcode
		public static boolean isValidParanthesis(String s) {
			Stack<Character> stack = new Stack<Character>();
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				switch (c) {
				case '(':
				case '[':
				case '{':
					stack.add(c);
					break;
				case ')':
					if (stack.size() == 0 || stack.pop() != '(') {
						return false;
					}
					;
					break;
				case ']':
					if (stack.size() == 0 || stack.pop() != '[') {
						return false;
					}
					;
					break;
				case '}':
					if (stack.size() == 0 || stack.pop() != '{') {
						return false;
					}
					;
					break;
				default:
					continue;
				}
			}// for i
			if (stack.size() == 0) {
				return true;
			} else {
				return false;
			}

		}

		// accepted solution on leetcode
		public static String addBinary(String a, String b) {
			if (a.length() == 0 || a == null) {
				return b;
			}
			if (b.length() == 0 || b == null) {
				return a;
			}
			StringBuilder builder = new StringBuilder();
			int carry = 0;
			StringBuilder buildera = new StringBuilder();
			buildera.append(a);
			String str1 = buildera.reverse().toString();
			StringBuilder builderb = new StringBuilder();
			builderb.append(b);
			String str2 = builderb.reverse().toString();
			int i;

			// reverse a and b
			for (i = 0; i < str1.length() && i < str2.length(); i++) {

				int sum = Character.getNumericValue(str1.charAt(i))
						+ Character.getNumericValue(str2.charAt(i)) + carry;
				switch (sum) {
				case 0:
					builder.append(0);
					carry = 0;
					break;
				case 1:
					builder.append(1);
					carry = 0;
					break;
				case 2:
					builder.append(0);
					carry = 1;
					break;
				case 3:
					builder.append(1);
					carry = 1;
					break;

				}
			}
			if (i == str1.length()) {
				int j = i;
				for (; j < str2.length(); j++) {
					int sum = Character.getNumericValue(str2.charAt(j)) + carry;
					switch (sum) {
					case 0:
						builder.append(0);
						carry = 0;
						break;
					case 1:
						builder.append(1);
						carry = 0;
						break;
					case 2:
						builder.append(0);
						carry = 1;
						break;
					case 3:
						builder.append(1);
						carry = 1;
						break;

					}
				}

			} else if (i == str2.length()) {
				int j = i;
				for (; j < str1.length(); j++) {
					int sum = Character.getNumericValue(str1.charAt(j)) + carry;
					switch (sum) {
					case 0:
						builder.append(0);
						carry = 0;
						break;
					case 1:
						builder.append(1);
						carry = 0;
						break;
					case 2:
						builder.append(0);
						carry = 1;
						break;
					case 3:
						builder.append(1);
						carry = 1;
						break;

					}
				}

			}
			if (carry == 1) {
				builder.append(carry);
			}
			builder.reverse();
			return builder.toString();

		}

		// accepted solution on leetcode.
		public static int strStrNeedleHaystack(String haystack, String needle) {
			if (needle == null || needle.length() == 0) {
				return -1;
			}
			for (int i = 0; i < haystack.length(); i++) {
				if (needle.length() - i > haystack.length()) {
					return -1;
				}
				int m = i;
				for (int j = 0; j < needle.length(); j++) {
					if (haystack.charAt(i) == needle.charAt(m)) {
						if (j == needle.length() - 1) {
							return i;
						}
						m++;
					} else {
						break;
					}
				}

			}
			return -1;
		}

		// this solution doesnt work if a char is present in both s1 and s2 and we
		// choose
		// randomly one string. it needs another implemntation.
		public static boolean isInterleave97(String s1, String s2, String s3) {
			if (s3.length() < s1.length() + s2.length()) {

				return false;
			}
			int i = 0;
			int j = 0;
			int k = 0;
			for (; i < s1.length() && j < s2.length();) {
				System.out.println("i = " + i + " j = " + j + " k = " + k + " "
						+ s3.charAt(k));
				if (s3.charAt(k) == s1.charAt(i)) {
					i++;
					k++;
				} else if (s3.charAt(k) == s2.charAt(j)) {
					j++;
					k++;
				} else {
					return false;
				}

			}
			if (i == s1.length()) {
				while (j < s2.length()) {
					System.out.println("s3.charAt(k): " + s3.charAt(k) + "k: " + k
							+ " s2.charAt(j)   " + s2.charAt(j) + "j: " + j);
					if (s3.charAt(k) != s2.charAt(j)) {
						return false;
					}
					j++;
					k++;

				}
				return true;
			} else if (j == s2.length()) {
				while (i < s1.length()) {
					if (s3.charAt(k) != s1.charAt(i)) {
						return false;
					}
					i++;
					k++;

				}
				return true;
			}
			return true;

		}

		// accepted solution on leetcode
		public static String reverseWords(String s) {

			if (s == null || s.length() == 0) {
				return "";
			}

			// split to words by space
			String[] arr = s.split(" ");
			StringBuilder sb = new StringBuilder();
			for (int i = arr.length - 1; i >= 0; --i) {
				if (!arr[i].equals("")) {
					sb.append(arr[i]).append(" ");
				}
			}
			return sb.length() == 0 ? "" : sb.substring(0, sb.length() - 1);
		}
		public static String multiply(String num1, String num2) {
			 String n1 = new StringBuilder(num1).reverse().toString();
			    String n2 = new StringBuilder(num2).reverse().toString();
			 
			    int[] d = new int[num1.length()+num2.length()];
			 
			    //multiply each digit and sum at the corresponding positions
			    for(int i=0; i<n1.length(); i++){
			        for(int j=0; j<n2.length(); j++){
			            d[i+j] += (n1.charAt(i)-'0') * (n2.charAt(j)-'0');
			        }
			    }
			 
			    StringBuilder sb = new StringBuilder();
			 
			    //calculate each digit
			    for(int i=0; i<d.length; i++){
			        int mod = d[i]%10;
			        int carry = d[i]/10;
			        if(i+1<d.length){
			            d[i+1] += carry;
			        }
			        sb.insert(0, mod);
			    }
			 
			    //remove front 0's
			    while(sb.charAt(0) == '0' && sb.length()> 1){
			        sb.deleteCharAt(0);
			    }
			 
			    return sb.toString();
		}

		public static String longestPalindrome(String s) {
			return null;
		}

		//leetcode accepted solution
		public static String convertZigZag6(String s, int numRows) {
			if (s == null || s.length() == 0 || numRows == 0) {
				return "";
			}
			int weidth = findWidthForZigZag(s, numRows);
			System.out.println(weidth);
			char[][] arr = new char[numRows][weidth];
			int col = -1;
			int i = 0;
			while (i < s.length()) {
				// for filling staright line
				col++;
				for (int j = 0; j < numRows && i < s.length(); j++) {
					arr[j][col] = s.charAt(i);
					i++;
				}
				// for filling diagonal line
				for (int j = numRows - 2; j > 0 && i < s.length(); j--) {
					col++;
					arr[j][col] = s.charAt(i);
					i++;

				}
			}
			StringBuilder builder = new StringBuilder();
			for (int m = 0; m < arr.length; m++) {
				for (int n = 0; n < arr[m].length; n++) {
					if (arr[m][n] != '\0') {
						builder.append(arr[m][n]);
					}

				}
			}
			return builder.toString();
		}

		private static int findWidthForZigZag(String s, int r) {
			if (r <= 0) {
				return 0;
			}

			int n = s.length();
			if(n<=r){
			    return 1;
			}
			int numOfZigZag = n / (r + (r - 2));
			int remainLength = n % (r + (r - 2));
			int weidthOfOneZigZag = 1 + (r - 2);
			int weidthOfCompleteZigZag = numOfZigZag * weidthOfOneZigZag;
			int finalWeidth = weidthOfCompleteZigZag;
			
			if (remainLength == 0) {
				return finalWeidth;
			}
			if (remainLength < r) {
				finalWeidth = finalWeidth + 1;
			} else {
				finalWeidth = finalWeidth + 1;
				finalWeidth = finalWeidth + (remainLength - r);
			}
			return finalWeidth;

		}

		// Accepted in leetcode
		public static String reverseString344(String s) {
			if (s == null || s.length() == 0) {
				return "";
			}
			StringBuilder builder = new StringBuilder();
			for (int i = s.length() - 1; i >= 0; i--) {
				builder.append(s.charAt(i));
			}
			return builder.toString();

		}

		// Accepted in leetcode
		public static String reverseVowels345(String s) {
			if (s == null || s.length() == 0) {
				return "";
			}
			StringBuilder builder = new StringBuilder();
			StringBuilder vowelbuilder = new StringBuilder();
			StringBuilder finalBuilder = new StringBuilder();

			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) == 'a' || s.charAt(i) == 'e' || s.charAt(i) == 'i'
						|| s.charAt(i) == 'o' || s.charAt(i) == 'u'
						|| s.charAt(i) == 'A' || s.charAt(i) == 'E'
						|| s.charAt(i) == 'I' || s.charAt(i) == 'O'
						|| s.charAt(i) == 'U') {
					builder.append(s.charAt(i));
				}

			}
			String vowels = builder.toString();
			for (int i = vowels.length() - 1; i >= 0; i--) {
				vowelbuilder.append(vowels.charAt(i));
			}
			String reversedVowels = vowelbuilder.toString();
			for (int i = 0, j = 0; i < s.length(); i++) {
				if (s.charAt(i) == 'a' || s.charAt(i) == 'e' || s.charAt(i) == 'i'
						|| s.charAt(i) == 'o' || s.charAt(i) == 'u'
						|| s.charAt(i) == 'A' || s.charAt(i) == 'E'
						|| s.charAt(i) == 'I' || s.charAt(i) == 'O'
						|| s.charAt(i) == 'U') {
					finalBuilder.append(reversedVowels.charAt(j));
					j++;

				} else {
					finalBuilder.append(s.charAt(i));
				}
			}
			return finalBuilder.toString();

		}

		// Accepted in leetcode
		public static boolean canConstruct383(String ransomNote, String magazine) {
			Map<Character, Integer> magazineMap = new HashMap<Character, Integer>();
			for (int i = 0; i < magazine.length(); i++) {
				if (magazineMap.containsKey(magazine.charAt(i))) {
					int count = magazineMap.get(magazine.charAt(i));
					magazineMap.put(magazine.charAt(i), ++count);
				} else {
					magazineMap.put(magazine.charAt(i), 1);
				}

			}
			for (int i = 0; i < ransomNote.length(); i++) {
				char current = ransomNote.charAt(i);
				if (!magazineMap.containsKey(current)) {
					return false;
				}
				int count = magazineMap.get(current);
				System.out.print(count);
				if (count <= 0) {
					return false;
				}
				magazineMap.put(current, --count);

			}
			return true;

		}

}
