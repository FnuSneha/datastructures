package string;

public class ReverseWords151 {
	public String reverseWords(String s) {
		char[] arr = s.toCharArray();
		return reverseWordsHelper(arr);
	}
	
	private void reverseRange(char[] arr, int start, int end) {
		int mid = (start + end) / 2;
		for (int i = start; i < mid; i++) {
			char temp = arr[i];
			arr[i] = arr[end - i - 1 + start];
			arr[end - i - 1 + start] = temp;
		}
	}
	
	private void shiftLeft(char[] arr, int len) {
		for (int i = 0; i < arr.length - len; i++) {
			arr[i] = arr[i + len];
		}
		for (int i = arr.length - 1 - len; i < arr.length; i++) {
			arr[i] = ' ';
		}
	}
	
	private void shiftRight(char[] arr, int len) {
		for (int i = arr.length - len - 1; i >= 0; i--) {
			arr[i + len] = arr[i];
		}
		for (int i = 0; i < len; i++) {
			arr[i] = ' ';
		}
	}
	
	private void exchangeSurroundingSpaces(char[] arr) {
		int i = 0, j = arr.length - 1;
		while (i < arr.length && Character.isWhitespace(arr[i])) { i++; }
		while (j >= 0 && Character.isWhitespace(arr[j])) { j--; }
		int rightSpaces = arr.length - 1 - j;
		int leftSpaces = i;
		if (leftSpaces > rightSpaces) shiftLeft(arr, leftSpaces - rightSpaces);
		if (rightSpaces > leftSpaces) shiftRight(arr, rightSpaces - leftSpaces);
	}

	private String reverseWordsHelper(char[] arr) {
		reverseRange(arr, 0, arr.length);
		exchangeSurroundingSpaces(arr);
		boolean inWord = false;
		int wordStart = 0;
		for (int i = 0; i < arr.length; i++) {
			boolean isWhitespace = Character.isWhitespace(arr[i]);
		
			if (isWhitespace && inWord) {
				reverseRange(arr, wordStart, i);
			}
			if (!isWhitespace && !inWord) {
				wordStart = i;
			}
			inWord = !isWhitespace;
		}
		if (inWord) {
			reverseRange(arr, wordStart, arr.length);
		}
		return new String(arr);
	}

}
