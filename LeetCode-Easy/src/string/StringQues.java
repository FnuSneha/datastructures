package string;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class StringQues {
	static HashMap<Character, String> map = new HashMap<Character, String>();

	public static int romanToInt(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}
		String str = s.toUpperCase();
		int prev = 0;
		int sum = 0;

		for (int i = str.length() - 1; i >= 0; i--) {
			char curr = str.charAt(i);
			switch (curr) {
			case 'M':
				sum = processDecimal(prev, 1000, sum);
				prev = 1000;
				break;

			case 'D':
				sum = processDecimal(prev, 500, sum);
				prev = 500;
				break;

			case 'C':
				sum = processDecimal(prev, 100, sum);
				prev = 100;
				break;

			case 'L':
				sum = processDecimal(prev, 50, sum);
				prev = 50;
				break;

			case 'X':
				sum = processDecimal(prev, 10, sum);
				prev = 10;
				break;

			case 'V':
				sum = processDecimal(prev, 5, sum);
				prev = 5;
				break;

			case 'I':
				sum = processDecimal(prev, 1, sum);
				prev = 1;
				break;
			}

		}
		return sum;
	}

	private static int processDecimal(int prev, int curr, int sum) {
		if (prev > curr) {
			return sum - curr;
		} else
			return sum + curr;

	}

	public static int myAtoiStringToInteger(String str) {
		if (str == null || str.length() < 1)
			return 0;

		// trim white spaces
		str = str.trim();

		char flag = '+';

		// check negative or positive
		int i = 0;
		if (str.charAt(0) == '-') {
			flag = '-';
			i++;
		} else if (str.charAt(0) == '+') {
			i++;
		}
		// use double to store result
		double result = 0;
		for (; i < str.length() && str.charAt(i) >= '0' && str.charAt(i) <= '9'; i++) {
			result = result * 10 + (str.charAt(i) - '0');
		}

		if (flag == '-')
			result = -result;

		// handle max and min
		if (result > Integer.MAX_VALUE)
			return Integer.MAX_VALUE;

		if (result < Integer.MIN_VALUE)
			return Integer.MIN_VALUE;

		return (int) result;

	}

	public static String longestCommonPrefix(String[] strs) {
		if (strs == null || strs.length == 0) {
			return null;
		}
		int maxLength = findMinLenArray(strs) + 1;
		for (int j = 0; j < strs.length - 1; j++) {
			for (int i = 0; i < maxLength; i++) {
				if (strs[j] != null || strs[j].length() != 0) {
					if (strs[j].charAt(i) != (strs[j + 1].charAt(i))) {
						return strs[j].substring(0, i);
					}
				}

			}
		}
		return strs[0].substring(0, maxLength);

	}

	private static int findMinLenArray(String[] arr) {
		if (arr == null || arr.length == 0) {
			return 0;
		}

		int min = arr[0].length();
		for (int i = 0; i < arr.length; i++) {
			if (min > arr[i].length()) {
				min = arr[i].length();
			}
		}
		return min;

	}

	// accepted soultion on leetcode
	public static int lengthOfLongestSubstringWithoutRepeatChars(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}
		int maxCount = 0;
		for (int i = 0; i < s.length(); i++) {
			Set<Character> set = new HashSet<Character>();
			int count = 0;
			for (int j = i; j < s.length(); j++) {
				if (!set.contains(s.charAt(j))) {
					set.add(s.charAt(j));
					count++;
				} else {
					break;
				}

			}
			if (maxCount < count) {
				maxCount = count;
			}

		}
		return maxCount;
	}

	public static String simplifyPathLinuxFile(String path) {
		if (path == null || path.charAt(0) != '/') {
			return null;
		}
		if (path.length() == 0) {
			return "/";
		}

		StringBuilder builder = new StringBuilder();
		Stack<String> stack = new Stack<String>();
		String[] list = path.split("/");
		for (String s : list) {
			if (s.equals("..") && !stack.isEmpty())
				stack.pop();
			else if (!s.equals(".") && !s.isEmpty()) {
				stack.push(s);
			}
		}
		if (stack.isEmpty()) {
			return "/";
		} else {
			while (!stack.isEmpty()) {
				builder.insert(0, "/" + stack.pop());
			}

		}

		return builder.toString();
	}

	public static int numDecodings91(String s) {
		if (s == null) {
			return 0;
		}
		if (s.length() == 0) {
			return 0;
		}
		return numDecodings91H(s);

	}

	private static boolean isValid(String s) {
		if (s.length() == 0)
			return false;
		int i = Integer.parseInt(s);
		if (i >= 1 && i <= 26)
			return true;
		else
			return false;
	}

	private static int numDecodings91H(String s) {
		if (s.length() == 0) {
			return 1;
		}
		if (s.length() == 1) {
			if (isValid(s)) {
				return 1;
			} else {
				return 0;
			}

		}
		int num1 = 0, num2 = 0;
		if (isValid(Character.toString(s.charAt(0)))) {
			num1 += numDecodings91H(s.substring(1));
		}
		if (isValid(Character.toString(s.charAt(0))
				+ Character.toString(s.charAt(1)))) {
			num2 += numDecodings91H(s.substring(2));
		}
		return num1 + num2;
	}

	// sk anu wats wrong
	public static List<String> letterCombinations(String digits) {
		List<String> result = new ArrayList<String>();
		if (digits == null) {
			return null;
		}
		if (digits.length() == 0) {
			return result;
		}
		map.put('2', new String("abc"));
		map.put('3', new String("def"));
		map.put('4', new String("ghi"));
		map.put('5', new String("jkl"));
		map.put('6', new String("mno"));
		map.put('7', new String("pqrs"));
		map.put('8', new String("tuv"));
		map.put('9', new String("wxyz"));
		return letterCombinationsHelper("", digits);
	}

	private static List<String> letterCombinationsHelper(String s1, String s2) {
		if (s2.length() == 0) {
			List<String> myRecList = new ArrayList<String>();
			myRecList.add(s1);
			return myRecList;
		}
		List<String> myBigList = new ArrayList<String>();

		String s4 = map.get(s2.charAt(0));
		for (int j = 0; j < s4.length(); j++) {
			myBigList.addAll(letterCombinationsHelper(s1 + s4.charAt(j),
					s2.substring(1)));
		}

		return myBigList;

	}

	public static List<String> findAllCombiPair(String s) {
		if (s == null || s.length() == 0) {
			return null;
		}
		return findAllCombiPairH("abc", "");
	}

	private static List<String> findAllCombiPairH(String s1, String s2) {
		if (s1.length() == 2) {
			List<String> myRecList = new ArrayList<String>();
			myRecList.add(s1);
			return myRecList;
		}
		List<String> myBigList = new ArrayList<String>();
		for (int i = 0; i < s2.length(); i++) {
			myBigList.addAll(findAllCombiPairH(s1 + s2.charAt(i),
					s2.substring(0, i) + s2.substring(i + 1)));

		}
		return myBigList;

	}

	public static List<String> findAllCombi(String str) {
		if (str == null || str.length() == 0) {
			return null;
		}
		return findAllCombiHelper("", str);
	}

	public static List<String> findAllCombiPrefix(String pre, String str) {
		if (str == null || str.length() == 0) {
			return null;
		}
		return findAllCombiPrefixHelper(pre, str);
	}

	private static List<String> findAllCombiPrefixHelper(String s1, String s2) {
		if (s2.length() == 0) {
			List<String> myRecList = new ArrayList<String>();
			myRecList.add(s1);
			return myRecList;
		}
		List<String> myBigList = new ArrayList<String>();
		for (int i = 0; i < s2.length(); i++) {
			myBigList.addAll(findAllCombiHelper(s1 + s2.charAt(i),
					s2.substring(0, i) + s2.substring(i + 1)));

		}
		return myBigList;
	}

	private static List<String> findAllCombiHelper(String s1, String s2) {
		if (s2.length() == 0) {
			List<String> myRecList = new ArrayList<String>();
			myRecList.add(s1);
			return myRecList;
		}
		List<String> myBigList = new ArrayList<String>();
		for (int i = 0; i < s2.length(); i++) {
			myBigList.addAll(findAllCombiHelper(s1 + s2.charAt(i),
					s2.substring(0, i) + s2.substring(i + 1)));

		}
		return myBigList;
	}
}
