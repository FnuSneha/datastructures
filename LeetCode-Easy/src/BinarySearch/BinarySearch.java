package BinarySearch;

import java.util.HashSet;

public class BinarySearch {


	int guess(int num) {
		return 2;
	}

	public int guessNumber(int n) {
		int low = 1;
		int high = n;

		while (low <= high) {
			int mid = low + ((high - low) / 2);
			int result = guess(mid);
			if (result == 0) {
				return mid;
			} else if (result == 1) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}

		return -1;
	}

	boolean isBadVersion(int version) {
		return false;
	}

	public int firstBadVersion(int n) {
		if (n <= 1) {
			return n;
		}
		return firstBadVersionHelper(1, n);
	}

	public int firstBadVersionHelper(int start, int end) {
		int middle = start + (end - start) / 2;
		if (start >= end) {
			return start;
		}
		if (isBadVersion(middle)) {
			return firstBadVersionHelper(start, middle);
		} else {
			return firstBadVersionHelper(middle + 1, end);
		}

	}



	public int binarySearchIterative(int[] a, int x) {
		int low = 0;
		int high = a.length - 1;
		while (low <= high) {
			int mid = (low + high) / 2;
			if (a[mid] == x)
				return mid;
			else if (a[mid] < x)
				low = mid + 1;
			else
				high = mid - 1;
		}
		return -1;
	}

	public int binarySearchRecursive(int[] a, int x) {
		return binarySearch(a, x, 0, a.length - 1);
	}

	// need extra low and high parameters
	private int binarySearch(int[] a, int x, int low, int high) {
		if (low > high)
			return -1;
		int mid = (low + high) / 2;
		if (a[mid] == x)
			return mid;
		else if (a[mid] < x)
			return binarySearch(a, x, mid + 1, high);
		else
			// last possibility: a[mid] > x
			return binarySearch(a, x, low, mid - 1);
	}
}
