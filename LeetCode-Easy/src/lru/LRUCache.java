package lru;

import java.util.LinkedHashMap;

/**
 * @author Sneha
 *
 * @param <K>
 * @param <V>
 */
public class LRUCache<K, V> {
	LinkedHashMap<K, V> map = new LinkedHashMap<>(16, 0.75f, true);
	int maxSize;

	public LRUCache(int max_size) {
		super();
		this.maxSize = max_size;
	}

	public synchronized void put(K key, V value) {
		if (map.containsKey(key)) {
			map.put(key, value);
		} else {
			trimToSize();
			map.put(key, value);
		}

	}

	private synchronized void trimToSize() {
		// please remove eldest until size is less than max size.
	}

	public synchronized V get(K key) {
		return map.get(key);
	}

}
