package union;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class UnionQues {
	int[] leader = null;

	private List<Integer> isCapableOfConnectingBoard(char[][] grid, int i, int j) {
		int rowLength = grid.length;
		int colLength = grid[0].length;
		List<Integer> neighbourList = new ArrayList<Integer>();
		if (i + 1 < rowLength && grid[i + 1][j] == '0' && j + 1 < colLength
				&& grid[i][j + 1] == '0') {
			neighbourList.add(i + 1);
			neighbourList.add(j);
			neighbourList.add(i);
			neighbourList.add(j + 1);
			return neighbourList;
		}
		if (i + 1 < rowLength && grid[i + 1][j] == '0') {
			neighbourList.add(i + 1);
			neighbourList.add(j);
			return neighbourList;
		}
		if (j + 1 < colLength && grid[i][j + 1] == '0') {
			neighbourList.add(i);
			neighbourList.add(j + 1);
			return neighbourList;
		}
		return neighbourList;
	}

	public void solve(char[][] board) {
		if (board == null || board.length == 0 || board[0].length == 0)
			return;
		if(board[0][0]=='0'){
			return;
		}
		leader = new int[board.length * board[0].length];
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] == 'X') {
					leader[(i * board[0].length) + j] = -1;
				} else {
					leader[(i * board[0].length) + j] = (i * board[0].length)
							+ j;
				}

			}
		}

		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] != 'X') {
					List<Integer> neighbourList = isCapableOfConnectingBoard(
							board, i, j);
					int x = (i * board[0].length) + j;
					if (neighbourList.size() == 4) {
						int i1 = neighbourList.get(0);
						int j1 = neighbourList.get(1);
						int i2 = neighbourList.get(2);
						int j2 = neighbourList.get(3);
						int y1 = (i1 * board[0].length) + j1;
						int y2 = (i2 * board[0].length) + j2;
						union(x, y1);
						union(x, y2);
					}
					if (neighbourList.size() == 2) {
						int i1 = neighbourList.get(0);
						int j1 = neighbourList.get(1);
						int y1 = (i1 * board[0].length) + j1;
						union(x, y1);

					}

				}
			}
		}

		Set<Integer> rootSet = new HashSet<Integer>();
		for (int k = 0; k < leader.length; k++) {
			if (leader[k] == k) {
				rootSet.add(k);
			}
		}

		// int x = (i * board[0].length) + j;
		for (int j = 0; j < board[0].length; j++) {
			if (board[0][j] == '0') {
				rootSet.remove(find(j));
			}
		}
		for (int i = 0; i < board.length; i++) {
			if (board[i][0] == '0') {
				rootSet.remove(find(i * board[0].length));
			}
		}
		for (int j = 0; j < board[0].length; j++) {
			if (board[board.length - 1][j] == '0') {
				rootSet.remove(find(((board.length - 1) * board[0].length) + j));
			}
		}
		for (int i = 0; i < board.length; i++) {
			if (board[i][board[0].length - 1] == '0') {
				rootSet.remove(find((i * board[0].length)
						+ (board[0].length - 1)));
			}
		}

		List<Integer> indexListToChange = new ArrayList<Integer>();
		for (int k = 0; k < leader.length; k++) {
			if (leader[k] != -1) {
				if (rootSet.contains(find(leader[k]))) {
					int i = k / board[0].length;
					int j = k % board[0].length;
					indexListToChange.add(i);
					indexListToChange.add(j);

				}
			}

		}
		if (indexListToChange == null || indexListToChange.size() == 0) {
			return;
		}
		int i;
		for (i = 0; i < indexListToChange.size() - 2;) {
			int x = indexListToChange.get(i);
			int y = indexListToChange.get(i + 1);
			board[x][y] = 'X';
			i = i + 2;

		}

		if (i == indexListToChange.size() - 2 && i==0) {
			int x = indexListToChange.get(indexListToChange.size() - 1);
			int y = indexListToChange.get(indexListToChange.size() - 2);
			board[x][y] = 'X';
		}
		for (int m = 0; m < board.length; m++) {
			for (int j = 0; j < board[m].length; j++) {
				System.out.print(board[m][j]);
			}
		}

	}

	public int numIslands(char[][] grid) {
		if (grid == null || grid.length == 0 || grid[0].length == 0)
			return 0;
		leader = new int[grid.length * grid[0].length];
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[0].length; j++) {
				if (grid[i][j] == '0') {
					leader[(i * grid[0].length) + j] = -1;
				} else {
					leader[(i * grid[0].length) + j] = (i * grid[0].length) + j;
				}

			}
		}

		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[0].length; j++) {
				if (grid[i][j] != '0') {
					List<Integer> neighbourList = isCapableOfConnecting(grid,
							i, j);
					int x = (i * grid[0].length) + j;
					if (neighbourList.size() == 4) {
						int i1 = neighbourList.get(0);
						int j1 = neighbourList.get(1);
						int i2 = neighbourList.get(2);
						int j2 = neighbourList.get(3);
						int y1 = (i1 * grid[0].length) + j1;
						int y2 = (i2 * grid[0].length) + j2;
						union(x, y1);
						union(x, y2);
					}
					if (neighbourList.size() == 2) {
						int i1 = neighbourList.get(0);
						int j1 = neighbourList.get(1);
						int y1 = (i1 * grid[0].length) + j1;
						union(x, y1);

					}

				}
			}
		}
		int count = 0;
		for (int i = 0; i < leader.length; i++) {
			if (leader[i] == i) {
				count++;
			}
		}

		return count;
	}

	private List<Integer> isCapableOfConnecting(char[][] grid, int i, int j) {
		int rowLength = grid.length;
		int colLength = grid[0].length;
		List<Integer> neighbourList = new ArrayList<Integer>();
		if (i + 1 < rowLength && grid[i + 1][j] == '1' && j + 1 < colLength
				&& grid[i][j + 1] == '1') {
			neighbourList.add(i + 1);
			neighbourList.add(j);
			neighbourList.add(i);
			neighbourList.add(j + 1);
			return neighbourList;
		}
		if (i + 1 < rowLength && grid[i + 1][j] == '1') {
			neighbourList.add(i + 1);
			neighbourList.add(j);
			return neighbourList;
		}
		if (j + 1 < colLength && grid[i][j + 1] == '1') {
			neighbourList.add(i);
			neighbourList.add(j + 1);
			return neighbourList;
		}
		return neighbourList;
	}

	// find number of disconnected components.
	public int countComponents(int n, int[][] edges) {
		leader = new int[n];

		for (int i = 0; i < n; i++) {
			leader[i] = i;
		}
		for (int i = 0; i < edges.length; i++) {
			union(edges[i][0], edges[i][1]);
		}
		int count = 0;
		for (int i = 0; i < leader.length; i++) {
			if (leader[i] == i) {
				count++;
			}
		}
		System.out.println("printing leader: ");
		return count;
	}

	/*
	 * private int find(int key) { if (leader[key] == key) { return key; }
	 * leader[key] = find(leader[key]); return leader[key]; }
	 */
	private int find(int key) {
		Stack<Integer> stack = new Stack<Integer>();
		while (key != leader[key]) {
			stack.push(key);
			key = leader[key];

		}
		int root = key;

		while (!stack.isEmpty()) {
			leader[stack.pop()] = root;

		}
		return key;

	}

	private void union(int x, int y) {
		leader[find(x)] = find(y);
	}
}
