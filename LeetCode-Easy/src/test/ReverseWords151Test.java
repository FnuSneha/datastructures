package test;

import org.junit.Assert;
import org.junit.Test;

import string.ReverseWords151;

public class ReverseWords151Test {
	@Test
	public void testReverseSimpleCase() {
		String input = "This is good day";
		Assert.assertEquals("day good is This", new ReverseWords151().reverseWords(input));
	}
	
	@Test
	public void testReverseOneWord() {
		String input = "This";
		Assert.assertEquals("This", new ReverseWords151().reverseWords(input));
	}
	
	@Test
	public void testReverseEmptyString() {
		String input = "";
		Assert.assertEquals("", new ReverseWords151().reverseWords(input));
	}
	
	@Test
	public void testReverseOnlySpaces() {
		String input = "   ";
		Assert.assertEquals("   ", new ReverseWords151().reverseWords(input));
	}
	
	@Test
	public void testReverseSurroundingSpaces() {
		String input = "    This is good ";
		Assert.assertEquals("    good is This ", new ReverseWords151().reverseWords(input));
	}
}
