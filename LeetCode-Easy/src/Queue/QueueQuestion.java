package Queue;

import java.util.LinkedList;
import java.util.Queue;

public class QueueQuestion {
	public class MovingAvgWindow {
		Queue<Integer> queue = new LinkedList<Integer>();
		int windowSize;
		double avg;

		public MovingAvgWindow(int size) {
			this.windowSize = size;

		}

		public double next(int val) {
			if (queue.size() < windowSize) {
				queue.offer(val);
				int sum = 0;
				for (int i : queue) {
					sum = sum + i;
				}
				return (double) sum / queue.size();
			} else {
				int head = queue.poll();
				double minus = (double) head / windowSize;
				queue.offer(val);
				double plus = (double) val / windowSize;
				return (double) avg + plus - minus;

			}

		}

	}

}
