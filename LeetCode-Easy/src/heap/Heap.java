package heap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;



public class Heap {
	class Prime {
		int prime;
		int index;

		public Prime(int prime, int index) {
			super();
			this.prime = prime;
			this.index = index;
		}

	}

	// ask anurag
	public int nthSuperUglyNumber(int n, int[] primes) {

		List<Integer> res = new LinkedList<>();
		PriorityQueue<Prime> heap = new PriorityQueue<Prime>(
				new Comparator<Prime>() {

					@Override
					public int compare(Prime p1, Prime p2) {

						return (p1.prime * res.get(p1.index))
								- (p2.prime * res.get(p2.index));
					}
				});

		for (int i = 0; i < primes.length; i++) {
			heap.offer(new Prime(primes[i], 1)); // prime and index

		}
		res.add(1);
		
		System.out.println(res);
		return res.get(res.size() - 1);

	}

	// 1,7,11 and 2,4,6
	public List<int[]> kSmallestPairs(int[] nums1, int[] nums2, int k) {
		List<int[]> res = new LinkedList<>();

		if ((nums1 == null || nums1.length == 0)
				|| (nums2 == null || nums2.length == 0) || k <= 0) {
			return res;
		}
		PriorityQueue<int[]> heap = new PriorityQueue<int[]>(
				new Comparator<int[]>() {

					@Override
					public int compare(int[] p1, int[] p2) {

						return (nums1[p1[0]] + nums2[p1[1]])
								- (nums1[p2[0]] + nums2[p2[1]]);
					}
				});
		heap.offer(new int[] { 0, 0 }); // to ensure heap has one min thing
		while (k > 0 && !heap.isEmpty()) {
			int[] arr = heap.poll();
			int i = arr[0];
			int j = arr[1];
			res.add(new int[] { nums1[i], nums2[j] });
			if (j + 1 < nums2.length) {
				heap.offer(new int[] { i, j + 1 });// jo poll kia wo ek contri
													// karega
			}
			if (i + 1 < nums1.length && j == 0) {
				heap.offer(new int[] { i + 1, 0 });// nums1 ka sara elemnts ka
													// j=0 dalo to ensure
				// intializing heap types
			}
			k--;

		}

		return res;
	}

	class Sum {
		int val;
		int i;
		int j;

		public Sum(int val, int i, int j) {
			this.val = val;
			this.i = i;
			this.j = j;
		}
	}

	public int kthSmallest(int[][] matrix, int k) {
		if (matrix == null || matrix.length == 0 || k <= 0) {
			return -1;
		}
		PriorityQueue<Sum> heap = new PriorityQueue<Sum>(new Comparator<Sum>() {

			@Override
			public int compare(Sum s1, Sum s2) {
				// TODO Auto-generated method stub
				return s1.val - s2.val;
			}
		});
		int result = matrix[0][0];
		for (int j = 0; j < matrix.length; j++) {
			heap.offer(new Sum(matrix[j][0], j, 0));
		}

		while (k > 0 && !heap.isEmpty()) {
			Sum s = heap.poll();
			int i = s.i;
			int j = s.j;
			result = matrix[i][j];
			if (j + 1 < matrix[i].length) {
				heap.offer(new Sum(matrix[i][j + 1], i, j + 1));
			}

			k--;
		}
		return result;
	}

	class Pair {
		int num;
		int count;

		public Pair(int num, int count) {
			this.num = num;
			this.count = count;
		}
	}

	public List<Integer> topKFrequent(int[] nums, int k) {
		// count the frequency for each element
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int num : nums) {
			if (map.containsKey(num)) {
				map.put(num, map.get(num) + 1);
			} else {
				map.put(num, 1);
			}
		}

		// create a min heap
		PriorityQueue<Pair> queue = new PriorityQueue<Pair>(
				new Comparator<Pair>() {
					public int compare(Pair a, Pair b) {
						return a.count - b.count;
					}
				});

		// maintain a heap of size k.
		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			Pair p = new Pair(entry.getKey(), entry.getValue());
			queue.offer(p);
			if (queue.size() > k) {
				queue.poll();
			}
		}

		// get all elements from the heap
		List<Integer> result = new ArrayList<Integer>();
		while (queue.size() > 0) {
			result.add(queue.poll().num);
		}
		// reverse the order
		Collections.reverse(result);

		return result;
	}
	  public int findKthLargest(int[] nums, int k) {
	      PriorityQueue<Integer> q = new PriorityQueue<Integer>(k);
			for (int i : nums) {
				q.offer(i);
			}
			int size = q.size();
			for (int i = 0; i < size - k; i++) {
				System.out.println("times removed: ");
				q.poll();
			}

			return q.peek();
	    }
}
