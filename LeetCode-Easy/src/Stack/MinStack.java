package Stack;

import java.util.Stack;

//accepted solution on leetcode.
public class MinStack {
	Stack<Integer> stack1;
	Stack<Integer> stack2;

	public MinStack() {
		stack1 = new Stack<Integer>();
		stack2 = new Stack<Integer>();
	}

	public void push(int x) {
		stack1.push(x);
		if (stack1.size() == 0) {
			stack2.push(x);
		} else {
			if (x == Math.min(x, stack2.peek())) {
				stack2.push(x);
			}
		}

	}

	public void pop() {
		if (stack1.size() != 0) {
			int popElement = stack1.pop();
			if (stack2.peek() == popElement) {
				stack2.pop();
			}
		}else{
			return;
		}

	}

	public int top() {
		if (stack1.size() != 0) {
			return stack1.peek();
		} else {
			return -1;
		}
	}

	public int getMin() {
		if (stack2.size() != 0) {
			return stack2.peek();
		} else {
			return -1;
		}

	}
}
