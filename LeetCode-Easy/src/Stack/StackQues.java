package Stack;

import java.util.Stack;

public class StackQues {
	//Valid paranthesis
	public boolean isValid(String s) {
		Stack<Character> stack = new Stack<Character>();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			switch (c) {
			case '(':
			case '[':
			case '{':
				stack.add(c);
				break;
			case ')':
				if (stack.size() == 0 || stack.pop() != '(') {
					return false;
				}
				;
				break;
			case ']':
				if (stack.size() == 0 || stack.pop() != '[') {
					return false;
				}
				;
				break;
			case '}':
				if (stack.size() == 0 || stack.pop() != '{') {
					return false;
				}
				;
				break;
			default:
				continue;
			}
		}
		if (stack.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}
	
	
}
