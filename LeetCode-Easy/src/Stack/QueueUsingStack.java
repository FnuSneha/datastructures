package Stack;

import java.util.Stack;

//accepted soltuion on leetcode
public class QueueUsingStack {
	Stack<Integer> stack1 = new Stack<Integer>();;
	Stack<Integer> stack2 = new Stack<Integer>();;

	

	public void push(int x) {
		stack1.push(x);

	}

	// Removes the element from in front of queue.
	public void pop() {
		if (!stack2.isEmpty()) {
			stack2.pop();
			return;
		} else {
			while (!stack1.isEmpty()) {
				stack2.push(stack1.pop());
			}

		}
		if (!stack2.isEmpty()) {
			stack2.pop();
		}

	}

	// Get the front element.
	public int peek() {
		if (!stack2.isEmpty()) {
			return stack2.peek();
		} else {
			while (!stack1.isEmpty()) {
				stack2.push(stack1.pop());
			}

		}
		if (!stack2.isEmpty()) {
			return stack2.peek();
		}
		throw new IllegalArgumentException("stack empty");
	}

	// Return whether the queue is empty.
	public boolean empty() {
		return stack1.size() == 0 && stack2.size() == 0;
	}

}
