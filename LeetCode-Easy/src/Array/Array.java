package Array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class Array {
	public int removeElement(int[] nums, int val) {
		if (nums == null || nums.length == 0) {
			return 0;
		}
		int i = 0;
		int j = 0;
		while (j < nums.length) {
			if (nums[j] != val) {
				nums[i] = nums[j];
				i++;
			}
			j++;

		}
		return i;
	}

	public List<List<Integer>> threeSum(int[] nums) {
		return threeSumTarget(nums, 0);
	}

	public List<List<Integer>> threeSumTarget(int[] nums, int target) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();

		if (nums == null || nums.length < 3)
			return result;

		Arrays.sort(nums);
		for (int i = 0; i < nums.length - 2; i++) {
			if (i == 0 || nums[i] > nums[i - 1]) {

				int j = i + 1;
				int k = nums.length - 1;

				while (j < k) {
					if (nums[i] + nums[j] + nums[k] == target) {
						List<Integer> l = new ArrayList<Integer>();
						l.add(nums[i]);
						l.add(nums[j]);
						l.add(nums[k]);
						result.add(l);

						j++;
						k--;

						// handle duplicate here
						while (j < k && nums[j] == nums[j - 1])
							j++;
						while (j < k && nums[k] == nums[k + 1])
							k--;

					} else if (nums[i] + nums[j] + nums[k] < target) {
						j++;
					} else {
						k--;
					}
				}

			}
		}

		return result;

	}

	public int[] twoSum(int[] nums, int target) {
		int[] result = new int[2];
		if (nums == null || nums.length < 2) {
			return result;
		}
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < nums.length; i++) {
			if (map.containsKey(nums[i])) {
				result[0] = map.get(nums[i]);
				result[1] = i;
				return result;

			}
			map.put(target - nums[i], i);
		}
		return result;
	}

	public List<List<Integer>> fourSum(int[] nums, int target) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();

		if (nums == null || nums.length < 4)
			return result;

		Arrays.sort(nums);

		for (int i = 0; i < nums.length - 3; i++) {
			if (i != 0 && nums[i] == nums[i - 1])
				continue;
			for (int j = i + 1; j < nums.length - 2; j++) {
				if (j != i + 1 && nums[j] == nums[j - 1])
					continue;
				int k = j + 1;
				int l = nums.length - 1;
				while (k < l) {
					if (nums[i] + nums[j] + nums[k] + nums[l] < target) {
						k++;
					} else if (nums[i] + nums[j] + nums[k] + nums[l] > target) {
						l--;
					} else {
						List<Integer> t = new ArrayList<Integer>();
						t.add(nums[i]);
						t.add(nums[j]);
						t.add(nums[k]);
						t.add(nums[l]);
						result.add(t);

						k++;
						l--;

						while (k < l && nums[l] == nums[l + 1]) {
							l--;
						}

						while (k < l && nums[k] == nums[k - 1]) {
							k++;
						}
					}

				}
			}
		}

		return result;
	}

	public void rotate(int[] arr, int order) {
		if (arr == null || arr.length == 0 || order < 0) {
			throw new IllegalArgumentException("Illegal argument!");
		}

		if (order > arr.length) {
			order = order % arr.length;
		}
		int firstArrL = arr.length - order;
		reverse(arr, 0, firstArrL - 1);
		reverse(arr, firstArrL, arr.length - 1);
		reverse(arr, 0, arr.length - 1);

	}

	private void reverse(int[] arr, int left, int right) {
		if (arr == null || arr.length == 0) {
			return;
		}
		while (left < right) {
			int temp = arr[right];
			arr[right] = arr[left];
			arr[left] = temp;
			left++;
			right--;
		}

	}

	public int thirdMax(int[] nums) {
		if (nums == null || nums.length == 0) {
			throw new IllegalArgumentException("Illegal argument!");
		}
		Set<Integer> set = new HashSet<Integer>();
		PriorityQueue<Integer> queue = new PriorityQueue<Integer>();
		for (int i = 0; i < nums.length; i++) {
			if (!set.contains(nums[i])) {
				set.add(nums[i]);
				if (queue.size() < 3) {
					queue.offer(nums[i]);
				} else {
					int num = queue.peek();
					if (num < nums[i]) {
						queue.poll();
						queue.offer(nums[i]);
					}

				}

			}

		}
		if (queue.size() == 3) {
			return queue.peek();
		} else {
			while (queue.size() > 1) {
				queue.poll();
			}
		}
		return queue.peek();

	}

	public int majorityElement(int[] num) {
		if (num.length == 1) {
			return num[0];
		}

		Arrays.sort(num);
		return num[num.length / 2];
	}

	public boolean containsDuplicate(int[] nums) {
		if (nums == null || nums.length < 2) {
			return false;
		}
		Set<Integer> set = new HashSet<Integer>();
		for (int i = 0; i < nums.length; i++) {
			if (set.contains(nums[i])) {
				return true;
			}
			set.add(nums[i]);
		}
		return false;
	}

	public int removeDuplicatesSortedArray(int[] A) {
		if (A.length < 2)
			return A.length;

		int i = 0;
		int j = 1;

		while (j < A.length) {
			if (A[i] == A[j]) {
				j++;
			} else {
				i++;
				A[i] = A[j];
				j++;
			}
		}

		return i + 1;
	}

	public boolean containsNearbyDuplicate(int[] nums, int k) {
		if (nums == null || nums.length < 2 || k == 0 || k > nums.length) {
			return false;
		}
		for (int i = 0; i < nums.length - 1; i++) {
			if (i + k >= nums.length) {
				return false;
			}
			for (int j = i + 1; j < k; j++) {
				if (nums[i] == nums[j]) {
					return true;
				}

			}
		}
		return false;
	}

	// this is when [1,1,1,2,2,3] we leave behind 1, 1, 2, 2 ,3.
	public int removeDuplicatesLeavingTwo(int[] A) {
		if (A.length <= 2)
			return A.length;

		int prev = 1; // point to previous
		int curr = 2; // point to current
		while (curr < A.length) {
			if (A[curr] == A[prev] && A[curr] == A[prev - 1]) {
				curr++;
			} else {
				prev++;
				A[prev] = A[curr];
				curr++;
			}

		}
		return prev + 1;
	}

	// pascal traingle 2
	public List<Integer> pascalTrainglegGetRow(int rowIndex) {
		ArrayList<Integer> result = new ArrayList<Integer>();

		if (rowIndex < 0)
			return result;

		result.add(1);
		for (int i = 1; i <= rowIndex; i++) {
			for (int j = result.size() - 2; j >= 0; j--) {
				result.set(j + 1, result.get(j) + result.get(j + 1));
			}
			result.add(1);
		}
		return result;
	}

	public List<ArrayList<Integer>> pascalTrainglegenerate(int numRows) {
		List<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		if (numRows <= 0)
			return result;

		ArrayList<Integer> pre = new ArrayList<Integer>();
		pre.add(1);
		result.add(pre);

		for (int i = 2; i <= numRows; i++) {
			ArrayList<Integer> cur = new ArrayList<Integer>();

			cur.add(1); // first
			for (int j = 0; j < pre.size() - 1; j++) {
				cur.add(pre.get(j) + pre.get(j + 1)); // middle
			}
			cur.add(1);// last

			result.add(cur);
			pre = cur;
		}

		return result;
	}

	public int[] plusOne(int[] digits) {
		if (digits == null) {
			int[] sum = {};
			return sum;
		}
		if (digits.length == 0) {
			int[] sum = { 1 };
			return sum;
		}
		int carry = 0;
		int[] sum = new int[digits.length + 1];
		for (int i = digits.length - 1; i >= 0; i--) {
			if (i == digits.length - 1) {
				sum[i + 1] = (digits[i] + 1 + carry) % 10;
				if (((digits[i] + 1 + carry)) / 10 == 1) {
					carry = 1;
				} else {
					carry = 0;
				}
			} else {
				sum[i + 1] = (digits[i] + carry) % 10;
				if (((digits[i] + carry)) / 10 == 1) {
					carry = 1;
				} else {
					carry = 0;
				}
			}

		}
		if (carry == 1) {
			sum[0] = 1;
		} else {
			int arr[] = new int[sum.length - 1];
			for (int i = 1; i < sum.length; i++) {
				arr[i - 1] = sum[i];
			}
			return arr;

		}

		return sum;

	}

	public void moveZeroes(int[] nums) {
		if (nums == null || nums.length == 0) {
			return;
		}
		int i = 0;
		int j = 0;
		while (j < nums.length) {
			if (nums[j] == 0) {
				j++;
			} else {
				int temp = nums[j];
				nums[j] = nums[i];
				nums[i] = temp;
				i++;
				j++;
			}

		}
	}

	public void mergeSortedArray(int A[], int m, int B[], int n) {

		while (m > 0 && n > 0) {
			if (A[m - 1] > B[n - 1]) {
				A[m + n - 1] = A[m - 1];
				m--;
			} else {
				A[m + n - 1] = B[n - 1];
				n--;
			}
		}

		while (n > 0) {
			A[m + n - 1] = B[n - 1];
			n--;
		}
	}

	public int shortestWordDistance(String[] words, String word1, String word2) {
		int m = -1;
		int n = -1;

		int min = Integer.MAX_VALUE;

		for (int i = 0; i < words.length; i++) {
			String s = words[i];
			if (word1.equals(s)) {
				m = i;
				if (n != -1)
					min = Math.min(min, m - n);
			} else if (word2.equals(s)) {
				n = i;
				if (m != -1)
					min = Math.min(min, n - m);
			}
		}

		return min;
	}

	public int stockMaxProfit(int[] prices) {
		if (prices == null || prices.length <= 1) {
			return 0;
		}
		int profit = 0;
		int minCp = prices[0];
		for (int i = 1; i < prices.length; i++) {
			profit = Math.max(profit, prices[i] - minCp);
			minCp = Math.min(minCp, prices[i]);

		}
		return profit;
	}

	public List<String> findMissingRanges(int[] nums, int lower, int upper) {
		List<String> result = new ArrayList<String>();
		if (nums == null || nums.length == 0) {
			outputToResult(lower, upper, result);
			return result;
		}
		// leading missing range
		if (nums[0] - lower > 0) {
			outputToResult(lower, nums[0] - 1, result);
		}

		for (int i = 1; i < nums.length; i++) {
			if (nums[i] - nums[i - 1] > 1) {
				outputToResult(nums[i - 1] + 1, nums[i] - 1, result);
			}
		}

		// trailing missage ranges
		if (upper - nums[nums.length - 1] > 0) {
			outputToResult(nums[nums.length - 1] + 1, upper, result);
		}
		return result;
	}

	private void outputToResult(int start, int end, List<String> result) {
		StringBuffer sb = new StringBuffer();
		if (start == end) {
			sb.append(start);
		} else {
			sb.append(start + "->" + end);
		}

		result.add(sb.toString());
	}

	public void setZeroes(int[][] matrix) {
		boolean firstRowZero = false;
		boolean firstColumnZero = false;

		// set first row and column zero or not
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[i][0] == 0) {
				firstColumnZero = true;
				break;
			}
		}

		for (int i = 0; i < matrix[0].length; i++) {
			if (matrix[0][i] == 0) {
				firstRowZero = true;
				break;
			}
		}
		// mark zeros on first row and column
		for (int i = 1; i < matrix.length; i++) {
			for (int j = 1; j < matrix[0].length; j++) {
				if (matrix[i][j] == 0) {
					matrix[i][0] = 0;
					matrix[0][j] = 0;
				}
			}
		}

		// use mark to set elements
		for (int i = 1; i < matrix.length; i++) {
			for (int j = 1; j < matrix[0].length; j++) {
				if (matrix[i][0] == 0 || matrix[0][j] == 0) {
					matrix[i][j] = 0;
				}
			}
		}

		// set first column and row
		if (firstColumnZero) {
			for (int i = 0; i < matrix.length; i++)
				matrix[i][0] = 0;
		}

		if (firstRowZero) {
			for (int i = 0; i < matrix[0].length; i++)
				matrix[0][i] = 0;
		}

	}

	// for nums=1,2,3,4 first loop will make result as 24,12,4,1 and second loop
	// will
	// memorize
	public int[] productExceptSelf(int[] nums) {
		int[] result = new int[nums.length];
		result[nums.length - 1] = 1;
		for (int i = nums.length - 2; i >= 0; i--) {
			result[i] = result[i + 1] * nums[i + 1];
		}
		int left = 1;
		for (int i = 0; i < nums.length; i++) {
			result[i] = result[i] * left;
			left = left * nums[i];
		}
		return result;
	}

	public ArrayList<Integer> spiralOrder(int[][] matrix) {
		ArrayList<Integer> result = new ArrayList<Integer>();

		if (matrix == null || matrix.length == 0)
			return result;

		int m = matrix.length;
		int n = matrix[0].length;

		int x = 0;
		int y = 0;
		while (m > 0 && n > 0) {

			// if one row/column left, no circle can be formed
			if (m == 1) {
				for (int i = 0; i < n; i++) {
					result.add(matrix[x][y++]);
				}
				break;
			} else if (n == 1) {
				for (int i = 0; i < m; i++) {
					result.add(matrix[x++][y]);
				}
				break;
			}

			// below, process a circle

			// top - move right
			for (int i = 0; i < n - 1; i++) {
				result.add(matrix[x][y++]);
			}

			// right - move down
			for (int i = 0; i < m - 1; i++) {
				result.add(matrix[x++][y]);
			}

			// bottom - move left
			for (int i = 0; i < n - 1; i++) {
				result.add(matrix[x][y--]);
			}

			// left - move up
			for (int i = 0; i < m - 1; i++) {
				result.add(matrix[x--][y]);
			}

			x++;
			y++;
			m = m - 2;
			n = n - 2;
		}
		return result;

	}

	public boolean canJump(int[] A) {
		if (A.length <= 1)
			return true;

		int max = 0 + A[0]; // max stands for the largest index that can be
							// reached.

		for (int i = 0; i < A.length; i++) {
			if (max <= i && A[i] == 0)
				return false;
			// update max
			if (i + A[i] > max) {
				max = i + A[i];
			}
			// max is enough to reach the end
			if (max >= A.length - 1)
				return true;
		}
		return false;
	}

	public boolean sudokoExist(char[][] board, String word) {
		int m = board.length;
		int n = board[0].length;

		boolean result = false;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (dfs(board, word, i, j, 0)) {
					result = true;
				}
			}
		}
		return result;
	}

	private boolean dfs(char[][] board, String word, int i, int j, int k) {
		int m = board.length;
		int n = board[0].length;
		if (i < 0 || j < 0 || i >= m || j >= n) {
			return false;
		}
		if (board[i][j] == word.charAt(k)) {
			char temp = board[i][j];
			board[i][j] = '#';
			if (k == word.length() - 1) {
				return true;
			} else if (dfs(board, word, i - 1, j, k + 1)
					|| dfs(board, word, i + 1, j, k + 1)
					|| dfs(board, word, i, j - 1, k + 1)
					|| dfs(board, word, i, j + 1, k + 1)) {
				return true;
			}

			board[i][j] = temp;
		}
		return false;
	}

	public void sortColors(int[] nums) {
		if (nums == null || nums.length < 2) {
			return;
		}

		int[] countArray = new int[3];
		for (int i = 0; i < nums.length; i++) {
			countArray[nums[i]]++;
		}
		for (int i = 1; i <= 2; i++) {
			countArray[i] = countArray[i - 1] + countArray[i];
		}
		int zeroCountbeginIndx = countArray[0];
		int oneCountbeginIndx = countArray[1];
		int twoCountbeginIndx = countArray[2];
		int j = 0;
		for (; j < zeroCountbeginIndx; j++) {
			nums[j] = 0;
		}
		for (; j < oneCountbeginIndx; j++) {
			nums[j] = 1;
		}
		for (; j < twoCountbeginIndx; j++) {
			nums[j] = 2;
		}

	}
	// from top to bottn end of matrix,find min path sum.
		public int minPathSum(int[][] grid) {
			if (grid == null || grid.length == 0)
				return 0;

			int m = grid.length;
			int n = grid[0].length;

			int[][] dp = new int[m][n];
			dp[0][0] = grid[0][0];
			// initialize top row
			for (int i = 1; i < n; i++) {
				dp[0][i] = dp[0][i - 1] + grid[0][i];
			}

			// initialize left column
			for (int j = 1; j < m; j++) {
				dp[j][0] = dp[j - 1][0] + grid[j][0];
			}

			// fill up the dp table
			for (int i = 1; i < m; i++) {
				for (int j = 1; j < n; j++) {
					if (dp[i - 1][j] > dp[i][j - 1]) {
						dp[i][j] = dp[i][j - 1] + grid[i][j];
					} else {
						dp[i][j] = dp[i - 1][j] + grid[i][j];
					}
				}
			}

			return dp[m - 1][n - 1];

		}

	public int uniquePaths(int m, int n) {
		if (m == 0 || n == 0)
			return 0;
		if (m == 1 || n == 1)
			return 1;
		int[][] dp = new int[m][n];

		// initialize top row
		for (int i = 1; i < n; i++) {
			dp[0][i] = 1;
		}

		// initialize left column
		for (int j = 1; j < m; j++) {
			dp[j][0] = 1;
		}
		// fill up the dp table
		for (int i = 1; i < m; i++) {
			for (int j = 1; j < n; j++) {
				dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
			}
		}
		return dp[m - 1][n - 1];
	}

	public int uniquePathsWithObstacles(int[][] obstacleGrid) {
		if (obstacleGrid == null || obstacleGrid.length == 0)
			return 0;

		int m = obstacleGrid.length;
		int n = obstacleGrid[0].length;
		if (obstacleGrid[0][0] == 1 || obstacleGrid[m - 1][n - 1] == 1)
			return 0;
		int[][] dp = new int[m][n];
		dp[0][0] = 1;
		// left column
		for (int i = 1; i < m; i++) {
			if (obstacleGrid[i][0] == 1) {
				dp[i][0] = 0;
			} else {
				dp[i][0] = dp[i - 1][0];
			}
		}
		// top row
		for (int i = 1; i < n; i++) {
			if (obstacleGrid[0][i] == 1) {
				dp[0][i] = 0;
			} else {
				dp[0][i] = dp[0][i - 1];
			}
		}

		for (int i = 1; i < m; i++) {
			for (int j = 1; j < n; j++) {
				if (obstacleGrid[i][j] == 1) {
					dp[i][j] = 0;
				} else {
					dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
				}
			}
		}
		return dp[m - 1][n - 1];

	}

}
