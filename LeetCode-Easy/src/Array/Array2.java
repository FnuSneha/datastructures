package Array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Array2 {
	public boolean searchMatrix(int[][] matrix, int target) {
		if (matrix == null || matrix.length == 0 || matrix[0].length == 0)
			return false;

		int m = matrix.length;
		int n = matrix[0].length;

		int start = 0;
		int end = m * n - 1;

		while (start <= end) {
			int mid = (start + end) / 2;
			int midX = mid / n;// why this- aswk anu
			int midY = mid % n;// why this- aswk anu

			if (matrix[midX][midY] == target)
				return true;

			if (matrix[midX][midY] < target) {
				start = mid + 1;
			} else {
				end = mid - 1;
			}
		}

		return false;
	}

	public int missingNumber(int[] nums) {
		int sum = 0;
		for (int i = 0; i < nums.length; i++) {
			sum += nums[i];
		}
		int n = nums.length;
		int totalSumShouldBe = n * (n + 1) / 2;
		return totalSumShouldBe - sum;

	}

	// container with most water
	public int containerWithMaxWaterMaxArea(int[] height) {
		if (height == null || height.length < 2) {
			return 0;
		}

		int max = 0;
		int left = 0;
		int right = height.length - 1;
		while (left < right) {
			max = Math.max(max, Math.min(height[left], height[right])
					* (right - left));
			if (height[left] < height[right])
				left++;
			else
				right--;

		}

		return max;
	}

	// maxed out nikalo by reverse order ma kahan tak ascending ha. uskejis next
	// walla is
	// pigli. again reverse order ma find num just greater then pigli.
	// swap both and the reverse from pigle+1 till end of list.
	public void nextPermutation(int[] nums) {
		if (nums == null || nums.length < 2)
			return;
		int p = 0;
		for (int i = nums.length - 2; i >= 0; i--) {
			if (nums[i] < nums[i + 1]) {
				p = i;
				break;
			}
		}

		int q = 0;
		for (int i = nums.length - 1; i > p; i--) {
			if (nums[i] > nums[p]) {
				q = i;
				break;
			}
		}

		if (p == 0 && q == 0) {
			reverse(nums, 0, nums.length - 1);
			return;
		}
		int temp = nums[p];
		nums[p] = nums[q];
		nums[q] = temp;

		if (p < nums.length - 1) {
			reverse(nums, p + 1, nums.length - 1);
		}
	}

	private void reverse(int[] nums, int left, int right) {
		while (left < right) {
			int temp = nums[left];
			nums[left] = nums[right];
			nums[right] = temp;
			left++;
			right--;
		}
	}

	// find min in rotated sorted array
	public int findMinInRotatedSortedArray(int[] num) {
		int low = 0, high = num.length - 1;
		while (low < high && num[low] >= num[high]) {
			int mid = (low + high) / 2;
			if (num[mid] > num[high])
				low = mid + 1;
			else
				high = mid;
		}
		return num[low];
	}

	// max product subarray
	public int maxProduct(int[] nums) {
		int[] max = new int[nums.length];
		int[] min = new int[nums.length];

		max[0] = min[0] = nums[0];
		int result = nums[0];
		for (int i = 1; i < nums.length; i++) {
			if (nums[i] > 0) {
				max[i] = Math.max(nums[i], max[i - 1] * nums[i]);
				min[i] = Math.min(nums[i], min[i - 1] * nums[i]);
			} else {
				max[i] = Math.max(nums[i], min[i - 1] * nums[i]);
				min[i] = Math.min(nums[i], max[i - 1] * nums[i]);
			}
			result = Math.max(result, max[i]);
		}

		return result;
	}

	// peak elem jiske aage piche lower elem ho
	public int findPeakElement(int[] num) {
		int max = num[0];
		int index = 0;
		for (int i = 1; i <= num.length - 2; i++) {
			int prev = num[i - 1];
			int curr = num[i];
			int next = num[i + 1];

			if (curr > prev && curr > next && curr > max) {
				index = i;
				max = curr;
			}
		}

		if (num[num.length - 1] > max) {
			return num.length - 1;
		}

		return index;
	}

	// in this canditate nums can be repeated and and in combi each elem cant be
	// chosen more the once.

	public List<List<Integer>> combinationSum2(int[] candidates, int target) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();

		if (candidates == null || candidates.length == 0)
			return result;
		Arrays.sort(candidates);
		List<Integer> current = new ArrayList<Integer>();
		combinationSum2(candidates, target, 0, current, result);
		return result;
	}

	public void combinationSum2(int[] candidates, int target, int j,
			List<Integer> curr, List<List<Integer>> result) {
		if (target == 0) {
			ArrayList<Integer> temp = new ArrayList<Integer>(curr);
			result.add(temp);
			return;
		}
		int prev = -1;
		for (int i = j; i < candidates.length; i++) {
			if (prev != candidates[i]) { // each time start from different
											// element
				if (target < candidates[i])
					return;
				curr.add(candidates[i]);
				combinationSum2(candidates, target - candidates[i], i + 1,
						curr, result);
				curr.remove(curr.size() - 1);
				prev = candidates[i];
			}

		}
	}

	public ArrayList<ArrayList<Integer>> combinationSum(int[] candidates,
			int target) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();

		if (candidates == null || candidates.length == 0)
			return result;
		Arrays.sort(candidates);
		ArrayList<Integer> current = new ArrayList<Integer>();
		combinationSum(candidates, target, 0, current, result);
		return result;

	}

	// curr wo list ha jis ma ham utha utha kar ek ek element date hain aur uska
	// combi
	// mangte hain, j ha ye batane k lia ki curr ma wahan se aage se dalo so
	// that
	// unique combi genrate ho.
	public void combinationSum(int[] candidates, int target, int j,
			ArrayList<Integer> curr, ArrayList<ArrayList<Integer>> result) {
		if (target == 0) {
			ArrayList<Integer> temp = new ArrayList<Integer>(curr);
			result.add(temp);
			return;
		}
		for (int i = j; i < candidates.length; i++) {
			if (target < candidates[i])
				return;
			curr.add(candidates[i]);
			combinationSum(candidates, target - candidates[i], i, curr, result);
			curr.remove(curr.size() - 1);

		}
	}

	public int[] intersectionCommonInTwoArray(int[] nums1, int[] nums2) {
		HashSet<Integer> set1 = new HashSet<Integer>();
		for (int i : nums1) {
			set1.add(i);
		}

		HashSet<Integer> set2 = new HashSet<Integer>();
		for (int i : nums2) {
			if (set1.contains(i)) {
				set2.add(i);
			}
		}

		int[] result = new int[set2.size()];
		int i = 0;
		for (int n : set2) {
			result[i++] = n;
		}

		return result;
	}
}
