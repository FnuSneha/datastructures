package Driver;

import LinkList.LinkList;
import LinkList.LinkList.ListNode;

public class Driver {

	public static void main(String[] args) {
		LinkList question = new LinkList();
		ListNode head = null;
		ListNode head2 = null;
		head = question.insert(head, 10, 1);
		head = question.insert(head, 100, 2);
		head = question.insert(head, 1000, 3);
		question.printList(head);
		head2 = question.insert(head, 10, 1);
		head2 = question.insert(head, 100, 2);
		head2 = question.insert(head, 1000, 3);
		System.out.println("comparing list");
		System.out.println(question.compareList(head, head2));
		
		ListNode pal = question.insert(null, 1, 1);
		pal = question.insert(pal, 2, 2);
		pal = question.insert(pal, 3, 3);
		pal = question.insert(pal, 4, 4);
		question.printList(pal);
		System.out.println("\n");
		pal = question.swapPairs(pal);
		question.printList(pal);
		//question.printList(pal);
		//System.out.println("Palindrome: " + question.isPalindrome(pal));

	}

}
