package Sort;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Sort {
/*	public boolean canAttendMeetings(Interval[] intervals) {
		Arrays.sort(intervals, new Comparator<Interval>() {
			public int compare(Interval a, Interval b) {
				return a.start - b.start;
			}
		});

		for (int i = 0; i < intervals.length - 1; i++) {
			if (intervals[i].end > intervals[i + 1].start) {
				return false;
			}
		}

		return true;
	}*/

	public boolean isAnagram(String s, String t) {
		if (s == null || t == null || s.length() == 0 || t.length() == 0) {
			return false;
		}
		if (s.length() != t.length()) {
			return false;
		}
		List<Character> list1 = new LinkedList<Character>();
		List<Character> list2 = new LinkedList<Character>();
		for (int i = 0; i < s.length(); i++) {
			list1.add(s.charAt(i));
		}
		for (int i = 0; i < t.length(); i++) {
			list2.add(t.charAt(i));
		}
		Collections.sort(list1);
		Collections.sort(list2);
		if (list1.equals(list2)) {
			return true;
		}
		return false;

	}
}
