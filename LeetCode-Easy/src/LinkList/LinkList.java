package LinkList;

import java.util.Comparator;
import java.util.PriorityQueue;

public class LinkList {
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	public class ListNode {
		int val;
		ListNode next;

		public ListNode(int x) {
			val = x;
		}

		@Override
		public String toString() {
			return val + "->";
		}

	}

	public ListNode mergeTwoListsSorted(ListNode l1, ListNode l2) {
		ListNode head = new ListNode(0);
		ListNode p = head;

		while (l1 != null || l2 != null) {
			if (l1 != null && l2 != null) {
				if (l1.val < l2.val) {
					p.next = l1;
					l1 = l1.next;
				} else {
					p.next = l2;
					l2 = l2.next;
				}
				p = p.next;
			} else if (l1 == null) {
				p.next = l2;
				break;
			} else if (l2 == null) {
				p.next = l1;
				break;
			}
		}

		return head.next;
	}

	public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
		int len1 = 0;
		int len2 = 0;
		ListNode p1 = headA, p2 = headB;
		if (p1 == null || p2 == null)
			return null;

		while (p1 != null) {
			len1++;
			p1 = p1.next;
		}
		while (p2 != null) {
			len2++;
			p2 = p2.next;
		}

		int diff = 0;
		p1 = headA;
		p2 = headB;

		if (len1 > len2) {
			diff = len1 - len2;
			int i = 0;
			while (i < diff) {
				p1 = p1.next;
				i++;
			}
		} else {
			diff = len2 - len1;
			int i = 0;
			while (i < diff) {
				p2 = p2.next;
				i++;
			}
		}

		while (p1 != null && p2 != null) {
			if (p1.val == p2.val) {
				return p1;
			} else {

			}
			p1 = p1.next;
			p2 = p2.next;
		}

		return null;
	}

	public ListNode removeNthFromEnd(ListNode head, int n) {
		if (head == null)
			return null;

		ListNode fast = head;
		ListNode slow = head;

		for (int i = 0; i < n; i++) {
			fast = fast.next;
		}

		// if remove the first node
		if (fast == null) {
			head = head.next;
			return head;
		}

		while (fast.next != null) {
			fast = fast.next;
			slow = slow.next;
		}

		slow.next = slow.next.next;

		return head;
	}

	// deleting dups and keeping atleast one
	public ListNode deleteDuplicatesKeepOne(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode iterator = head;
		while (iterator != null) {
			ListNode iterator1 = iterator.next;
			while (iterator1 != null && iterator.val == iterator1.val) {
				iterator1 = iterator1.next;
			}
			iterator.next = iterator1;
			iterator = iterator1;
		}
		return head;

	}

	// dont leave even one trace of it
	public ListNode deleteDuplicatesDontLeaveAny(ListNode head) {
		ListNode t = new ListNode(0);
		t.next = head;

		ListNode p = t;
		while (p.next != null && p.next.next != null) {
			if (p.next.val == p.next.next.val) {
				int dup = p.next.val;
				while (p.next != null && p.next.val == dup) {
					p.next = p.next.next;
				}
			} else {
				p = p.next;
			}

		}

		return t.next;

	}

	// remove all elements when one val is given
	public ListNode removeElementsOneValGiven(ListNode head, int val) {
		if (head == null) {
			return head;
		}
		ListNode dummy = new ListNode(0);
		dummy.next = head;
		ListNode iterator = head;
		ListNode pre = dummy;
		while (iterator != null) {
			if (iterator.val == val) {
				pre.next = iterator.next;
			} else {
				pre = iterator;
			}
			iterator = iterator.next;

		}
		return dummy.next;

	}

	public ListNode swapPairs(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode dummy = new ListNode(0);
		dummy.next = head;

		ListNode pre = dummy;
		ListNode curr = head;
		while (curr != null && curr.next != null) {
			ListNode temp = curr.next.next;
			curr.next.next = curr;
			pre.next = curr.next;
			pre = curr;
			curr = temp;
		}
		pre.next = curr;
		return dummy.next;

	}

	public ListNode detectCycle(ListNode head) {
		ListNode fast = head;
		ListNode slow = head;

		while (fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;

			if (slow == fast) {
				break;
			}

		}
		if (fast == null || fast.next == null) {
			return null;
		}
		slow = head;
		while (slow != fast) {
			slow = slow.next;
			fast = fast.next;

		}

		return slow;
	}

	public boolean hasCycle(ListNode head) {
		ListNode fast = head;
		ListNode slow = head;

		while (fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;

			if (slow == fast)
				return true;
		}

		return false;
	}

	// delete when access to only node is given
	public void deleteNode(ListNode node) {
		node.val = node.next.val;
		node.next = node.next.next;
	}

	public boolean compareList(ListNode head1, ListNode head2) {
		System.out.println("FIRST: ");
		printList(head1);
		System.out.println("\nSecond: ");
		printList(head2);
		if (head1 == null || head2 == null) {
			return false;
		}
		ListNode temp1 = head1;
		ListNode temp2 = head2;
		while (temp1 != null && temp2 != null) {
			if (temp1.val != temp2.val) {
				return false;
			}

			temp1 = temp1.next;
			temp2 = temp2.next;

		}
		return temp1 == null && temp2 == null;

	}

	public boolean isPalindrome(ListNode head) {
		if (head == null || head.next == null) {
			return true;
		}
		ListNode slow = head;
		ListNode fast = head;
		ListNode slower = null;
		while (fast.next != null && fast.next.next != null) {
			fast = fast.next.next;
			slower = slow;
			slow = slow.next;
		}
		ListNode secondHead = slow.next;
		if (fast.next != null) {
			// Even case
			slow.next = null;
		} else {
			slower.next = null;
		}
		ListNode secondHeadReverse = reverseList(secondHead);

		return compareList(secondHeadReverse, head);

	}

	public ListNode insert(ListNode head, int value, int pos) {
		ListNode newNode = new ListNode(value);
		if (pos == 1) {
			newNode.next = head;
			head = newNode;
			return head;
		}

		ListNode temp = head;
		for (int i = 0; i < pos - 2; i++) {
			temp = temp.next;
		}
		newNode.next = temp.next;
		temp.next = newNode;
		return head;
	}

	public void printList(ListNode head) {
		if (head == null) {
			return;
		}
		ListNode temp = head;
		while (temp != null) {
			System.out.print(temp);
			temp = temp.next;
		}

	}

	// if both lists are already given in reversed way
	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		int carry = 0;

		ListNode newHead = new ListNode(0);
		ListNode p1 = l1, p2 = l2, p3 = newHead;

		while (p1 != null || p2 != null) {
			if (p1 != null) {
				carry += p1.val;
				p1 = p1.next;
			}

			if (p2 != null) {
				carry += p2.val;
				p2 = p2.next;
			}

			p3.next = new ListNode(carry % 10);
			p3 = p3.next;
			carry /= 10;
		}

		if (carry == 1)
			p3.next = new ListNode(1);

		return newHead.next;
	}

	public ListNode partitionLeftRightXGiven(ListNode head, int x) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode myHead = new ListNode(0);
		ListNode result = myHead;
		ListNode dummy = new ListNode(0);
		dummy.next = head;
		ListNode prev = dummy;
		ListNode curr = head;
		while (curr != null) {
			if (curr.val < x) {
				myHead.next = curr;
				myHead = myHead.next;
				prev.next = curr.next;
				curr = curr.next;
			} else {
				prev = curr;
				curr = curr.next;
			}

		}
		myHead.next = dummy.next;
		return result.next;

	}

	public ListNode oddEvenList(ListNode head) {
		if (head == null) {
			return head;
		}
		ListNode result = head;
		ListNode p1 = head;
		ListNode p2 = head.next;
		ListNode connectNode = head.next;
		while (p1 != null && p2 != null) {
			ListNode temp = p2.next;
			if (temp == null) {
				break;
			}
			p1.next = p2.next;
			p1 = p1.next;

			p2.next = p1.next;
			p2 = p2.next;

		}

		p1.next = connectNode;

		return result;

	}

	public ListNode rotateRight(ListNode head, int n) {
		if (head == null || n == 0)
			return head;
		int length = findLength(head);
		n = n % length;
		ListNode slow = head;
		ListNode fast = head;
		while (n > 0) {
			n--;
			fast = fast.next;
			if (fast == null) {
				break;
			}
		}
		if (fast == null || slow == fast)
			return head;
		while (fast.next != null) {
			fast = fast.next;
			slow = slow.next;
		}
		ListNode newHead = slow.next;
		slow.next = null;
		fast.next = head;
		return newHead;

	}

	// Given {1,2,3,4}, reorder it to {1,4,2,3}.
	public void reorderListStartEnd(ListNode head) {
		if (head == null || head.next == null) {
			return;
		}
		int length = findLength(head);
		ListNode iterator = head;
		int i = length / 2;
		while (i > 0) {
			iterator = iterator.next;
			i--;

		}
		ListNode toReverseHead = iterator.next;
		iterator.next = null;
		ListNode reversedList = reverseList(toReverseHead);

		ListNode p1 = head;
		ListNode p2 = reversedList;
		ListNode dummy = new ListNode(0);
		ListNode prev = dummy;
		while (p1 != null && p2 != null) {
			prev.next = p1;
			ListNode temp = p1.next;
			p1.next = p2;
			prev = p2;
			p1 = temp;
			p2 = p2.next;

		}
		if (p1 == null) {
			prev.next = p2;
		} else {
			prev.next = p1;
		}
		head = dummy.next;
	}

	public ListNode reverseBetween(ListNode head, int m, int n) {
		if (m < 1 || m >= n || head == null || n > findLength(head))
			return head;
		ListNode dummy = new ListNode(0);
		dummy.next = head;
		int i = 1;
		ListNode iterator = dummy;
		while (i < m) {
			iterator = iterator.next;
			i++;

		}
		ListNode FirstPart = iterator;
		ListNode toReverseHead = iterator.next;
		iterator.next = null;
		int j = m + 1;
		ListNode iterator2 = toReverseHead;
		while (j <= n) {
			iterator2 = iterator2.next;
			j++;
		}
		ListNode SecondPart = iterator2.next;
		iterator2.next = null;
		ListNode reversedList = reverseList(toReverseHead);
		ListNode reversedListLast = reversedList;
		toReverseHead.next = SecondPart;
		FirstPart.next = reversedList;
		return dummy.next;
	}

	private ListNode reverseList(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode current = head;
		ListNode prev = null;
		ListNode next;
		while (current != null) {
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;

		}
		head = prev;
		return head;

	}

	private int findLength(ListNode head) {
		if (head == null) {
			return 0;
		}
		ListNode iterator = head;
		int count = 0;
		while (iterator != null) {
			count++;
			iterator = iterator.next;

		}
		return count;

	}

	private ListNode getTail(ListNode head) {
		if (head == null) {
			return null;
		}

		while (head.next != null) {
			head = head.next;
		}
		return head;
	}

	private ListNode findMedian(ListNode head) {
		ListNode slow = head, fast = head.next;
		while (fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
		}
		return slow;
	}

	public ListNode sortList(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode mid = findMedian(head); // O(n)
		ListNode leftDummy = new ListNode(0), leftTail = leftDummy;
		ListNode rightDummy = new ListNode(0), rightTail = rightDummy;
		ListNode middleDummy = new ListNode(0), middleTail = middleDummy;
		while (head != null) {
			if (head.val < mid.val) {
				leftTail.next = head;
				leftTail = head;
			} else if (head.val > mid.val) {
				rightTail.next = head;
				rightTail = head;
			} else {
				middleTail.next = head;
				middleTail = head;
			}
			head = head.next;
		}
		leftTail.next = null;
		middleTail.next = null;
		rightTail.next = null;

		ListNode left = sortList(leftDummy.next);
		ListNode right = sortList(rightDummy.next);

		return concat(left, middleDummy.next, right);
	}

	private ListNode concat(ListNode left, ListNode middle, ListNode right) {
		ListNode dummy = new ListNode(0), tail = dummy;

		tail.next = left;
		tail = getTail(tail);
		tail.next = middle;
		tail = getTail(tail);
		tail.next = right;
		tail = getTail(tail);
		return dummy.next;
	}

	public TreeNode sortedListToBST(ListNode head) {
		if (head == null)
			return null;
		ListNode dummy = new ListNode(0);
		dummy.next = head;
		int len = findLength(head);
		return sortedListToBST(0, len - 1, dummy);
	}

	private TreeNode sortedListToBST(int start, int end, ListNode dum) {
		if (start > end)
			return null;

		// mid
		int mid = (start + end) / 2;

		TreeNode left = sortedListToBST(start, mid - 1, dum);
		TreeNode root = new TreeNode(dum.next.val);
		dum.next = dum.next.next;
		TreeNode right = sortedListToBST(mid + 1, end, dum);

		root.left = left;
		root.right = right;

		return root;
	}
	public ListNode mergeKLists(ListNode[] lists) {
	    if(lists==null||lists.length==0)
	        return null;
	 
	    PriorityQueue<ListNode> queue = new PriorityQueue<ListNode>(new Comparator<ListNode>(){
	        public int compare(ListNode l1, ListNode l2){
	            return l1.val - l2.val;
	        }
	    });
	 
	    ListNode head = new ListNode(0);
	    ListNode p = head;
	 
	    for(ListNode list: lists){
	        if(list!=null)
	            queue.offer(list);
	    }    
	 
	    while(!queue.isEmpty()){
	        ListNode n = queue.poll();
	        p.next = n;
	        p=p.next;
	 
	        if(n.next!=null)
	            queue.offer(n.next);
	    }    
	 
	    return head.next;
	 
	}
}
