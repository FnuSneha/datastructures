package Tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class Tree1 {
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	//257
	public List<String> binaryTreePathsFromRootToLeave(TreeNode root) {
		ArrayList<String> finalResult = new ArrayList<String>();

		if (root == null)
			return finalResult;

		ArrayList<String> curr = new ArrayList<String>();
		ArrayList<ArrayList<String>> results = new ArrayList<ArrayList<String>>();

		dfs(root, results, curr);

		for (ArrayList<String> al : results) {
			StringBuilder sb = new StringBuilder();
			sb.append(al.get(0));
			for (int i = 1; i < al.size(); i++) {
				sb.append("->" + al.get(i));
			}

			finalResult.add(sb.toString());
		}

		return finalResult;
	}

	public void dfs(TreeNode root, ArrayList<ArrayList<String>> list,
			ArrayList<String> curr) {
		curr.add(String.valueOf(root.val));

		if (root.left == null && root.right == null) {
			list.add(curr);
			return;
		}

		if (root.left != null) {
			ArrayList<String> temp = new ArrayList<String>(curr);
			dfs(root.left, list, temp);
		}

		if (root.right != null) {
			ArrayList<String> temp = new ArrayList<String>(curr);
			dfs(root.right, list, temp);
		}
	}
	
	//102

	public ArrayList<ArrayList<Integer>> levelOrder(TreeNode root) {
		ArrayList<ArrayList<Integer>> al = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> nodeValues = new ArrayList<Integer>();
		if (root == null)
			return al;

		LinkedList<TreeNode> current = new LinkedList<TreeNode>();
		LinkedList<TreeNode> next = new LinkedList<TreeNode>();
		current.add(root);

		while (!current.isEmpty()) {
			TreeNode node = current.remove();

			if (node.left != null)
				next.add(node.left);
			if (node.right != null)
				next.add(node.right);

			nodeValues.add(node.val);
			if (current.isEmpty()) {
				current = next;
				next = new LinkedList<TreeNode>();
				al.add(nodeValues);
				nodeValues = new ArrayList();
			}

		}
		return al;
	}

	//107
	public List<ArrayList<Integer>> levelOrderBottom(TreeNode root) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();

		if (root == null) {
			return result;
		}

		LinkedList<TreeNode> current = new LinkedList<TreeNode>();
		LinkedList<TreeNode> next = new LinkedList<TreeNode>();
		current.offer(root);

		ArrayList<Integer> numberList = new ArrayList<Integer>();

		// need to track when each level starts
		while (!current.isEmpty()) {
			TreeNode head = current.poll();

			numberList.add(head.val);

			if (head.left != null) {
				next.offer(head.left);
			}
			if (head.right != null) {
				next.offer(head.right);
			}

			if (current.isEmpty()) {
				current = next;
				next = new LinkedList<TreeNode>();
				result.add(numberList);
				numberList = new ArrayList<Integer>();
			}
		}

		// return Collections.reverse(result);
		ArrayList<ArrayList<Integer>> reversedResult = new ArrayList<ArrayList<Integer>>();
		for (int i = result.size() - 1; i >= 0; i--) {
			reversedResult.add(result.get(i));
		}

		return reversedResult;
	}
	//235
	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
	    if(root==null)
	        return null;
	 
	    if(root==p || root==q)
	        return root;
	 
	    TreeNode l = lowestCommonAncestor(root.left, p, q);
	    TreeNode r = lowestCommonAncestor(root.right, p, q);
	 
	    if(l!=null&&r!=null){
	        return root;
	    }else if(l==null&&r==null){
	        return null;
	    }else{
	        return l==null?r:l;
	    }
	}
	
	public TreeNode buildTreePREIN(int[] preorder, int[] inorder) {
		int preStart = 0;
		int preEnd = preorder.length - 1;
		int inStart = 0;
		int inEnd = inorder.length - 1;

		return construct(preorder, preStart, preEnd, inorder, inStart, inEnd);
	}

	private TreeNode construct(int[] preorder, int preStart, int preEnd,
			int[] inorder, int inStart, int inEnd) {
		if (preStart > preEnd || inStart > inEnd) {
			return null;
		}
		int val = preorder[preStart];
		TreeNode p = new TreeNode(val);
		// find parent element index from inorder
		int k = 0;
		for (int i = 0; i < inorder.length; i++) {
			if (val == inorder[i]) {
				k = i;
				break;
			}
		}
		// write recursion here passing new vals int l=k-inStart
		p.left = construct(preorder, preStart + 1, preStart + (k - inStart),
				inorder, inStart, k - 1);
		p.right = construct(preorder, preStart + (k - inStart) + 1, preEnd,
				inorder, k + 1, inEnd);

		return p;

	}

	public TreeNode buildTreeINPO(int[] inorder, int[] postorder) {
		int inStart = 0;
		int inEnd = inorder.length - 1;
		int postStart = 0;
		int postEnd = postorder.length - 1;

		return buildTree(inorder, inStart, inEnd, postorder, postStart, postEnd);
	}

	private TreeNode buildTree(int[] inorder, int inStart, int inEnd,
			int[] postorder, int postStart, int postEnd) {
		if (inStart > inEnd || postStart > postEnd)
			return null;

		int rootValue = postorder[postEnd];
		TreeNode root = new TreeNode(rootValue);

		int k = 0;
		for (int i = 0; i < inorder.length; i++) {
			if (inorder[i] == rootValue) {
				k = i;
				break;
			}
		}

		root.left = buildTree(inorder, inStart, k - 1, postorder, postStart,
				postStart + k - (inStart + 1));
		// Becuase k is not the length, it it need to -(inStart+1) to get the
		// length
		root.right = buildTree(inorder, k + 1, inEnd, postorder, postStart + k
				- inStart, postEnd - 1);
		// postStart+k-inStart = postStart+k-(inStart+1) +1

		return root;
	}
}
