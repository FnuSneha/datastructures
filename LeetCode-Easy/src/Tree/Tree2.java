package Tree;

import java.util.Stack;

import Tree.Tree1.TreeNode;

public class Tree2 {
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	// http://massivealgorithms.blogspot.com/2016/09/leetcode-404-sum-of-left-leaves.html
	public int sumOfLeftLeaves(TreeNode root) {
		int res = 0;

		Stack<TreeNode> stack = new Stack<>();
		stack.push(root);

		while (!stack.isEmpty()) {
			TreeNode node = stack.pop();
			if (node != null) {
				if (node.left != null && node.left.left == null
						&& node.left.right == null)
					res += node.left.val;
				stack.push(node.left);
				stack.push(node.right);
			}
		}

		return res;
	}

	// http://www.programcreek.com/2013/01/leetcode-path-sum/
	public boolean hasPathSum(TreeNode root, int sum) {
		if (root == null)
			return false;
		if (root.val == sum && (root.left == null && root.right == null))
			return true;

		return hasPathSum(root.left, sum - root.val)
				|| hasPathSum(root.right, sum - root.val);
	}

	public int minDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		int ldepth = minDepth(root.left);
		int rdepth = minDepth(root.right);
		if (ldepth == 0) {
			return 1 + rdepth;
		} else if (rdepth == 0) {
			return 1 + ldepth;
		}

		return (1 + Math.min(rdepth, ldepth));
	}

	public TreeNode invertTree(TreeNode root) {
		if (root != null) {
			helper(root);
		}

		return root;
	}

	private void helper(TreeNode p) {

		TreeNode temp = p.left;
		p.left = p.right;
		p.right = temp;

		if (p.left != null)
			helper(p.left);

		if (p.right != null)
			helper(p.right);
	}

	public boolean isSameTree(TreeNode p, TreeNode q) {
		if (p == null && q == null) {
			return true;
		} else if (p == null || q == null) {
			return false;
		}

		if (p.val == q.val) {
			return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
		} else {
			return false;
		}
	}

	public boolean isSymmetric(TreeNode root) {
		if (root == null)
			return true;
		return isSymmetric(root.left, root.right);
	}

	private boolean isSymmetric(TreeNode l, TreeNode r) {
		if (l == null && r == null) {
			return true;
		} else if (r == null || l == null) {
			return false;
		}

		if (l.val != r.val)
			return false;

		if (!isSymmetric(l.left, r.right))
			return false;
		if (!isSymmetric(l.right, r.left))
			return false;

		return true;
	}

	public int maxDepth(TreeNode root) {
		if (root == null)
			return 0;

		int leftDepth = maxDepth(root.left);
		int rightDepth = maxDepth(root.right);

		int bigger = Math.max(leftDepth, rightDepth);

		return bigger + 1;
	}
}
