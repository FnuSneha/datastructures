package thread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Looper {
	 boolean stop=false;
	 BlockingQueue<Runnable> queue=new LinkedBlockingQueue<Runnable>();
	
	public void stop(){
		stop=true;
	}
	public  void loop(){
		while(!stop){
			try {
				Runnable runable=queue.take();
				runable.run();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		
	}
	public void enQueue(Runnable runable){
		try {
			queue.put(runable);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
