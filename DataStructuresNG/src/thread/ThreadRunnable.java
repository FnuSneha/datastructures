package thread;

public class ThreadRunnable implements Runnable {
	String name;

	public ThreadRunnable(String name) {
		super();
		this.name = name;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("I am from thread :" + name);
		}

	}

}
