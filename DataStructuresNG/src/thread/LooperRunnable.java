package thread;

public class LooperRunnable implements Runnable{
	Looper looper;
	public LooperRunnable(Looper looper){
		this.looper=looper;
	}

	@Override
	public void run() {
		looper.loop();
		
	}

}
