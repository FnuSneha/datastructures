package questions.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

public class Leetcode {
	

	public List<String> findItinerary(String[][] tickets) {
		List<String> result = new LinkedList<String>();
		Map<String, PriorityQueue<String>> adjacyMap = new HashMap<String, PriorityQueue<String>>();
		for (int i = 0; i < tickets.length; i++) {
			if (!adjacyMap.containsKey(tickets[i][0])) {
				adjacyMap.put(tickets[i][0], new PriorityQueue<String>());
			}
			adjacyMap.get(tickets[i][0]).add(tickets[i][1]);
		}
		PriorityQueue<String> queue = null;
		// System.out.println(adjacyMap);
		if (adjacyMap != null) {
			queue = adjacyMap.get("JFK");
		}
		result.add("JFK");

		while (queue != null && !queue.isEmpty()) {
			result.add(queue.peek());
			if (adjacyMap != null) {
				queue = adjacyMap.get(queue.poll());
			}

		}

		return result;
	}

}
