package questions.greedy;

public class Leetcode {

	private String remove(String A, int pos) {
		return A.substring(0, pos) + A.substring(pos + 1, A.length());
	}

	// find first such number whose suceeding number is smaller
	// then him and then remove greater one ie. numbe itself and break; ex-1362.
	// 6 pehla num
	// jiske baad ek chot number aya.
	public String DeleteDigits(String A, int k) {
		if (A.length() == k) {
			return "";
		}

		for (int i = 0; i < k; i++) {
			for (int j = 0; j < A.length(); j++) {
				if (j == A.length() - 1 || A.charAt(j + 1) < A.charAt(j)) {
					A = remove(A, j);
					break;
				}
			}
		}

		int i = 0;
		while (i < A.length() - 1 && A.charAt(i) == '0') {
			i++;
		}
		return A.substring(i, A.length());
	}
}
