package questions.twitter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;

public class Twitter {
	HashMap<Integer, HashSet<Integer>> userMap;// user and followees
	HashMap<Integer, ArrayList<Integer>> tweetMap;// user and tweets
	HashMap<Integer, Integer> orderMap; // tweet and order
	int order; // global order counter

	/** Initialize your data structure here. */
	public Twitter() {
		userMap = new HashMap<Integer, HashSet<Integer>>();
		tweetMap = new HashMap<Integer, ArrayList<Integer>>();
		orderMap = new HashMap<Integer, Integer>();

	}

	/** Compose a new tweet. */
	public void postTweet(int userId, int tweetId) {
		ArrayList<Integer> list = tweetMap.get(userId);
		if (list == null) {
			list = new ArrayList<Integer>();
			tweetMap.put(userId, list);
		}
		list.add(tweetId);
		orderMap.put(tweetId, order++);
		follow(userId, userId);// follow himself
	}

	/**
	 * Retrieve the 10 most recent tweet ids in the user's news feed. Each item
	 * in the news feed must be posted by users who the user followed or by the
	 * user herself. Tweets must be ordered from most recent to least recent.
	 */
	class TweetOrderDS {
		int tweet;
		int order;

		public TweetOrderDS(int tweet, int order) {
			super();
			this.tweet = tweet;
			this.order = order;
		}

	}

	public List<Integer> getNewsFeed(int userId) {
		PriorityQueue<TweetOrderDS> heap = new PriorityQueue<TweetOrderDS>(
				new Comparator<TweetOrderDS>() {

					@Override
					public int compare(TweetOrderDS o1, TweetOrderDS o2) {
						// TODO Auto-generated method stub
						return o2.order - o1.order;
					}
				});
		List<Integer> result = new ArrayList<Integer>();
		HashSet<Integer> foloweeList;
		if (userMap != null) {
			foloweeList = userMap.get(userId);
			ArrayList<Integer> foloweeTweetList;
			if (foloweeList != null) {
				for (Integer folowee : foloweeList) {
					if (tweetMap != null) {
						foloweeTweetList = tweetMap.get(folowee);
						if (foloweeTweetList != null) {
							for (Integer tweet : foloweeTweetList) {
								if (orderMap != null) {
									int order = orderMap.get(tweet);
									heap.offer(new TweetOrderDS(tweet, order));
								}

							}
						}
					}

				}
			}
		}

		int n = 10;
		while (!heap.isEmpty() && n > 0) {
			result.add(heap.poll().tweet);
			n--;
		}
		return result;
	}

	/**
	 * Follower follows a followee. If the operation is invalid, it should be a
	 * no-op.
	 */
	public void follow(int followerId, int followeeId) {
		HashSet<Integer> set = userMap.get(followerId);
		if (set == null) {
			set = new HashSet<Integer>();
			userMap.put(followerId, set);
		}
		set.add(followeeId);
	}

	/**
	 * Follower unfollows a followee. If the operation is invalid, it should be
	 * a no-op.
	 */
	public void unfollow(int followerId, int followeeId) {
		if (followerId == followeeId)
			return;

		HashSet<Integer> set = userMap.get(followerId);
		if (set == null) {
			return;
		}

		set.remove(followeeId);
	}
}
