package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import api.Stack;
import api.exception.StackEmptyException;

public abstract class StackTest {
	public abstract Stack getInstance();
	
	@Test
	public void testStackPushPopTop() {
		Stack stack = getInstance();
		stack.push(15);
		stack.push(34);
		stack.push(30);
		try {
			assertEquals(30, stack.top());
			assertEquals(30, stack.pop());
			assertEquals(34, stack.pop());	
		} catch (StackEmptyException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}		
	}
	@Test(expected = StackEmptyException.class)
	public void testStackEmptyException() throws StackEmptyException{
		Stack stack = getInstance();
		stack.push(43);
		stack.push(34);
		stack.pop();
		stack.pop();
		stack.pop();
		
	}
	@Test
	public void testStackSize() {
		Stack stack = getInstance();
		stack.push(15);
		stack.push(34);
		stack.push(30);
		assertEquals(3, stack.size());
	}
	@Test
	public void testMaxCapacityOfStack() throws StackEmptyException{
		Stack stack = getInstance();
		for(int i=0;i<100;i++){
			stack.push(i);		
		}
		assertEquals(99, stack.top());
		assertEquals(99, stack.pop());
		assertEquals(98, stack.pop());
		assertEquals(97, stack.pop());
		stack.push(99999);
		assertEquals(99999, stack.pop());
		
	}
}
