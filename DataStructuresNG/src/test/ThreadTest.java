package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;

import thread.Handler;
import thread.Looper;
import thread.LooperRunnable;
import thread.ThreadRunnable;
import api.Counter;

public class ThreadTest {
	@Test
	public void testLooper () {
		int n = 1000;
		Looper looper=new Looper();
		LooperRunnable lr=new LooperRunnable(looper);
		Thread t=new Thread(lr);
		t.start();
		
		Handler h=new Handler(looper);
		
		for (int i = 0; i < n; i++) {
			ThreadRunnable runnable = new ThreadRunnable(String.valueOf(i));
			h.post(runnable);

		}
		
		
	}

/*	@Test
	public void testThreadPool() {
		int n = 1000;
		ExecutorService executor = Executors.newFixedThreadPool(10);
		for (int i = 0; i < n; i++) {
			ThreadRunnable runnable = new ThreadRunnable(String.valueOf(i));
			executor.execute(runnable);

		}
		executor.shutdown();
		while (!executor.isTerminated()) {
		}
		System.out.println("Finished all threads");

	}*/

	// for every new runnable u r making, u r making new thread and starting it.
	// means 1000 runnables,
	// 100 threads u start
	/*
	 * @Test public void testThread() {
	 * 
	 * List<Thread> threadList = new ArrayList<Thread>(); int n = 10; for (int i
	 * = 0; i < n; i++) { ThreadRunnable runnable = new
	 * ThreadRunnable(String.valueOf(i)); Thread t1 = new Thread(runnable);
	 * threadList.add(t1);
	 * 
	 * } for (Thread t : threadList) { t.start(); } for (Thread t : threadList)
	 * { try { t.join(); } catch (InterruptedException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } }
	 * 
	 * System.out.println("eveything has ended"); }
	 */
}
