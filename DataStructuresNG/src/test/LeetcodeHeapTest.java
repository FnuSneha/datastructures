package test;

import heap.Leetcode;

import java.util.List;

import org.junit.Test;

import questions.strings.Leetcode3;

public class LeetcodeHeapTest {
	@Test
	public void testkthLargest() {
		//int[] arr1 = { 1, 7, 11 };
		int[] arr2 = { 2, 7, 13, 19 };
		Leetcode leet = new Leetcode();

		int m = leet.nthSuperUglyNumber(12, arr2);
		System.out.println(m);
	}
}
