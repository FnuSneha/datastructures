package test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import questions.strings.Leetcode;
import string.Leetcode2;
import impl.ArrayBasedStack;
import api.Queue;
import api.Stack;

public class Leet2QuesStringTest {
	@Test
	public void testisPalindrome125() {
		// System.out.println(Leetcode2.isPalindrome125("sllsllssssll"));
	}

	@Test
	public void testpalindromePairs336() {
		String[] words = { "abcd", "dcba", "lls", "s", "sssll", "" };
		// System.out.println(Leetcode2.palindromePairs336(words));
	}

	@Test
	public void testlengthOfLastWord58() {
		// System.out.println(Leetcode2.lengthOfLastWord58("Hello World"));
	}

	@Test
	public void testcountAndSay38() {
		// System.out.println(Leetcode2.countAndSay38(1));
	}

	@Test
	public void testshortestPalindrome214() {

		System.out.println(Leetcode2.shortestPalindrome214("aaaaa"));
	}
}
