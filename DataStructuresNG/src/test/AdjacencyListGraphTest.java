package test;

import impl.AdjacencyListGraph;
import impl.ArrayBasedHeap;

import org.junit.Test;

import questions.graph.Leetcode;

public class AdjacencyListGraphTest {
	@Test
	public void testHeapInsert() {
		Leetcode leet = new Leetcode();
		String[][] tickets = { { "JFK", "SFO" }, { "JFK", "ATL" },
				{ "SFO", "ATL" }, { "ATL", "JFK" }, { "ATL", "SFO" } };
		System.out.println(leet.findItinerary(tickets));

	}
}
