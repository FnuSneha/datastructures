package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import api.Counter;

public abstract class CounterTest {

	public abstract Counter getInstance();

	@Test
	public void testCounter() {
		Counter counter = getInstance();
		assertEquals(0, counter.value());

		counter.increment();
		assertEquals(1, counter.value());
	}
}
