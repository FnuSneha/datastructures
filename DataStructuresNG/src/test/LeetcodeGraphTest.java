package test;

import impl.AdjacencyListGraph;
import impl.ArrayBasedHeap;

import org.junit.Test;

public class LeetcodeGraphTest {
	@Test
	public void testHeapInsert() {
		AdjacencyListGraph graph = new AdjacencyListGraph(7);
		graph.addEdge(1, 2);
		graph.addEdge(1, 7);
		graph.addEdge(1, 3);
		graph.addEdge(2, 3);
		graph.addEdge(3, 4);
		graph.addEdge(3, 5);
		graph.addEdge(4, 5);
		graph.addEdge(5, 6);
		graph.printGraph();
		System.out.println("Number of edges coming out of vertex 3 is: "
				+ graph.getNumberOfEdgesFromVertex(3));
		graph.BFT();
		graph.DFT();

	}
}
