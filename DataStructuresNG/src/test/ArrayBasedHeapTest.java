package test;

import impl.ArrayBasedHeap;

import org.junit.Test;

public class ArrayBasedHeapTest {
	@Test
	public void testHeapInsert() {
		ArrayBasedHeap minHeap = new ArrayBasedHeap(20);
		minHeap.insert(5);
		minHeap.insert(3);
		minHeap.insert(17);
		minHeap.insert(10);
		minHeap.insert(84);
		minHeap.insert(19);
		minHeap.insert(6);
		minHeap.insert(22);
		minHeap.insert(9);

		minHeap.print();
		minHeap.remove();
		System.out.println("After Removing once");
		minHeap.print();
	}
}
