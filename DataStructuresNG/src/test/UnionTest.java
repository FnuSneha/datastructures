package test;

import org.junit.Test;

import questions.strings.Leetcode3;
import union.Leetcode;

public class UnionTest {
	@Test
	public void testDisconnectedComponents() {
		Leetcode leet = new Leetcode();
		int[][] edges = { { 0, 1 }, { 1, 2 }, { 3, 4 }, { 4, 5 }, { 5, 6 },
				{ 7, 8 } };
		char[][] islands = { { '1', '1', '0', '0', '0' },
				{ '1', '1', '0', '0', '0' }, { '0', '0', '1', '0', '0' },
				{ '0', '0', '0', '1', '1' } };
		// System.out.println(leet.numIslands(islands));
	/*	char[][] board = { { 'X', 'X', 'X', 'X' }, { 'X', '0', '0', 'X' },
				{ 'X', 'X', '0', 'X' }, { 'X', '0', 'X', 'X' } };*/
		char[][] board={{0}};
		leet.solve(board);

	}
}
