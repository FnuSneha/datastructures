package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import api.Stack;
import api.Tree;
import api.exception.StackEmptyException;

public abstract class TreeTest {
	public abstract Tree getInstance();

	@Test
	public void testTreeDisplay() {
		Tree tree = getInstance();
		tree.insert(34);
		tree.insert(24);
		tree.insert(22);
		tree.insert(222);
		tree.insert(10);
		// tree.display();

	}

	@Test
	public void testTreeFind() {
		Tree tree = getInstance();
		tree.insert(34);
		tree.insert(24);
		tree.insert(22);
		tree.insert(222);
		tree.insert(10);
		assertEquals(true, tree.find(34));
		assertEquals(false, tree.find(3488));
	}

	@Test
	public void testTreeLeaves() {
		Tree tree = getInstance();
		tree.insert(34);
		tree.insert(24);
		tree.insert(22);
		tree.insert(222);
		tree.insert(10);

	}

	@Test
	public void testTreeBFS() {
		Tree tree = getInstance();
		tree.insert(7);
		tree.insert(4);
		tree.insert(12);
		tree.insert(2);
		tree.insert(6);
		tree.insert(9);
		tree.insert(19);
		tree.insert(3);
		tree.insert(5);
		tree.insert(8);
		tree.insert(11);
		tree.insert(15);
		tree.insert(20);
		tree.BFT();
		System.out.println("height is: " + tree.getHeight());

	}
}
