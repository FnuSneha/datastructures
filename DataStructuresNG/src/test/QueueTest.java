package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import api.Queue;
import api.exception.QueueEmptyException;

public abstract class QueueTest {
	public abstract Queue getInstance();
	@Test
	public void testQueueEnqueueDeque() {
		Queue queue = getInstance();
		queue.enqueue(15);
		queue.enqueue(16);
		queue.enqueue(17);
		try {
			assertEquals(15, queue.dequeue());
			assertEquals(16, queue.dequeue());
				
		} catch (QueueEmptyException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}		
	}
	/*@Test(expected = QueueEmptyException.class)
	public void testQueueEmptyException() throws QueueEmptyException{
		Queue queue = getInstance();
		queue.enqueue(15);
		queue.enqueue(16);
		queue.enqueue(17);
		try {
			queue.dequeue();
			queue.dequeue();
			queue.dequeue();
			queue.dequeue();
				
		} catch (QueueEmptyException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}	
		
	}*/
	@Test
	public void testQueueSize() {
		Queue queue = getInstance();
		queue.enqueue(15);
		queue.enqueue(16);
		queue.enqueue(17);
		assertEquals(3, queue.size());
	}

}
