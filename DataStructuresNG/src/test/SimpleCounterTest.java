package test;

import impl.SimpleCounter;
import api.Counter;

public class SimpleCounterTest extends CounterTest {
	@Override
	public Counter getInstance() {
		return new SimpleCounter();
	}
}
