package impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

import api.Graph;

public class AdjacencyListGraph implements Graph {
	Map<Integer, LinkedList<Integer>> adjacyMap;

	public AdjacencyListGraph(int number_of_vertices) {
		adjacyMap = new HashMap<Integer, LinkedList<Integer>>();
		for (int i = 1; i <= number_of_vertices; i++) {
			adjacyMap.put(i, new LinkedList<Integer>());
		}

	}

	@Override
	public void addEdge(int startVertex, int endVertex) {
		if (startVertex > adjacyMap.size() || endVertex > adjacyMap.size()) {
			System.out.println("Vertex not presnt in the graph");
			return;
		}
		LinkedList<Integer> startList = adjacyMap.get(startVertex);
		startList.add(endVertex);
		LinkedList<Integer> endList = adjacyMap.get(endVertex);
		endList.add(startVertex);
	}

	@Override
	public int getNumberOfVertices() {
		return adjacyMap.size();
	}

	@Override
	public List<Integer> getNumberOfEdgesFromVertex(int startVertex) {
		if (startVertex > adjacyMap.size()) {
			System.out.println("Vertex not presnt in the graph");
			return null;
		}
		return adjacyMap.get(startVertex);
	}

	@Override
	public void printGraph() {
		System.out.println("The graph is: ");
		System.out.println(adjacyMap);

	}

	class VertexLevel implements Comparable<VertexLevel> {
		int vertex;
		int level;

		public VertexLevel(int vertex, int level) {
			super();
			this.vertex = vertex;
			this.level = level;
		}

		@Override
		public String toString() {
			return "VertexLevel [vertex=" + vertex + ", level=" + level + "]";
		}

		@Override
		public int compareTo(VertexLevel o) {

			if (this.vertex < o.vertex)
				return -1;

			if (this.vertex > o.vertex)
				return 1;

			return 0;
		}

	}

	@Override
	public void BFT() {
		System.out.println("BFT of the graph is: ");
		Set<Integer> set = new HashSet<Integer>();
		Queue<VertexLevel> queue = new LinkedList<VertexLevel>();
		set.add((Integer) adjacyMap.keySet().toArray()[0]);
		int level = 0;
		VertexLevel v = new VertexLevel((int) adjacyMap.keySet().toArray()[0],
				level);
		queue.add(v);
		while (!queue.isEmpty()) {
			VertexLevel vertexLevel = queue.poll();
			System.out.println(vertexLevel);
			LinkedList<Integer> list = adjacyMap.get(vertexLevel.vertex);
			++level;
			boolean hasContri = false;
			for (Integer i : list) {
				if (!set.contains(i)) {
					VertexLevel v2 = new VertexLevel(i, level);
					queue.add(v2);
					set.add(i);
					hasContri = true;
				}

			}
			if (!hasContri) {
				--level;
			}
		}

	}

	@Override
	public void DFT() {
		System.out.println("DFT of the graph is: ");
		Set<Integer> set = new HashSet<Integer>();
		Stack<VertexLevel> stack = new Stack<VertexLevel>();
		List<VertexLevel> result = new ArrayList<VertexLevel>();
		set.add((Integer) adjacyMap.keySet().toArray()[0]);
		int level = 0;
		VertexLevel v = new VertexLevel((int) adjacyMap.keySet().toArray()[0],
				level);
		stack.add(v);
		while (!stack.isEmpty()) {
			VertexLevel vertexLevel = stack.pop();
			result.add(vertexLevel);
			LinkedList<Integer> list = adjacyMap.get(vertexLevel.vertex);
			++level;
			boolean hasContri = false;
			for (Integer i : list) {
				if (!set.contains(i)) {
					VertexLevel v2 = new VertexLevel(i, level);
					stack.add(v2);
					set.add(i);
					hasContri = true;
				}

			}
			if (!hasContri) {
				--level;
			}
		}
		System.out.println(result);

	}

}
