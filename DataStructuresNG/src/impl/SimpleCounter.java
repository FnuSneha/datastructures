package impl;

import api.Counter;

public class SimpleCounter implements Counter {
	private int value = 0;

	@Override
	public void increment() {
		value++;
	}

	@Override
	public int value() {
		return value;
	}
}
