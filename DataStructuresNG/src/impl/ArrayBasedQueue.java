package impl;

import api.Queue;
import api.exception.QueueEmptyException;

public class ArrayBasedQueue implements Queue {
	private final static int INTIAL_CAPACITY = 10;
	private int head;
	private int tail;
	private int size;

	Object[] queue = new Object[INTIAL_CAPACITY];

	private void resize(int capacity) {
		Object[] new_queue = new Object[capacity];
		for (int i = 0; i < size; i++) {
			new_queue[i] = queue[(i + head) % queue.length];
		}
		queue = new_queue;
		head = 0;
		tail = size;

	}

	@Override
	public void enqueue(Object obj) {
		if (size == queue.length) {
			resize(queue.length * 2);
		}
		queue[tail++] = obj;
		if (tail == queue.length) {
			tail = 0;
		}
		size++;

	}

	@Override
	public Object dequeue() throws QueueEmptyException {
		if (isEmpty()) {
			throw new QueueEmptyException();
		}
		Object to_deque = queue[head];
		queue[head] = null;
		head++;
		if (head == queue.length) {
			head = 0;
		}
		size--;
		if (size > 0 && size == queue.length / 4)
			resize(queue.length / 2);
		return to_deque;

	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size() < 0;
	}
}
