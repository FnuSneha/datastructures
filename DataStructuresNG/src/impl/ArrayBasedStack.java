package impl;

import api.Stack;
import api.exception.StackEmptyException;

public class ArrayBasedStack implements Stack {
	private static final int INITIAL_CAPACITY = 10;

	private int top = -1;
	Object[] stack = new Object[INITIAL_CAPACITY];

	@Override
	public void push(Object obj) {
		if (size() == stack.length) {
			Object[] new_Stack = new Object[2 * stack.length];
			int i = 0;
			for (; i < stack.length; i++) {
				new_Stack[i] = stack[i];
			}
			stack = new_Stack;

		}
		top++;
		stack[top] = obj;

	}

	@Override
	public Object pop() throws StackEmptyException {
		Object popping_element = top();
		stack[top] = null;
		top--;
		return popping_element;
	}

	@Override
	public boolean isEmpty() {
		return top < 0;
	}

	@Override
	public Object top() throws StackEmptyException {
		// TODO Auto-generated method stub
		if (top == -1) {
			throw new StackEmptyException();
		}
		return stack[top];
	}

	@Override
	public int size() {
		return top + 1;
	}

}
