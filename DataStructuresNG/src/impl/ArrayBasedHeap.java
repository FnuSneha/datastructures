package impl;

import api.Heap;

public class ArrayBasedHeap implements Heap {
	int[] arr;
	int max_size;
	int size;

	public ArrayBasedHeap(int max_size) {
		this.max_size = max_size;
		arr = new int[this.max_size + 1];
		this.size = 0;
		arr[0] = Integer.MIN_VALUE;
	}

	@Override
	public void insert(int data) {
		arr[++size] = data;
		int curr = size;
		while (arr[curr] < arr[getParent(curr)]) {
			swap(curr, getParent(curr));
			curr = getParent(curr);
		}

	}

	@Override
	public void print() {
		for (int i = 1; i <= size / 2; i++) {
			System.out.print(" PARENT : " + arr[i] + " LEFT CHILD : "
					+ arr[2 * i] + " RIGHT CHILD :" + arr[2 * i + 1]);
			System.out.println();
		}

	}

	private void heapify(int pos) {
		if (!isLeaf(pos)) {
			int leftChild = arr[getLeftChild(pos)];
			int rightChild = arr[getRightChild(pos)];
			if (arr[pos] > leftChild || arr[pos] > rightChild) {
				if (leftChild < rightChild) {
					swap(pos, getLeftChild(pos));
					heapify(getLeftChild(pos));
				} else {
					swap(pos, getRightChild(pos));
					heapify(getRightChild(pos));
					
				}

			}

		}

	}

	@Override
	public int remove() {
		int itemToBPooped = arr[1];
		arr[1] = arr[size--];
		heapify(1);
		return itemToBPooped;
	}

	@Override
	public boolean isLeaf(int pos) {
		return pos >= pos / 2 && pos <= size;
	}

	@Override
	public int getLeftChild(int pos) {
		return 2 * pos;
	}

	@Override
	public int getRightChild(int pos) {
		return 2 * pos + 1;
	}

	@Override
	public int getParent(int pos) {
		return pos / 2;
	}

	private void swap(int p1, int p2) {
		int temp = arr[p1];
		arr[p1] = arr[p2];
		arr[p2] = temp;
	}

	@Override
	public int getMin() {
		// TODO Auto-generated method stub
		return arr[1];
	}

	public int size() {
		return size;
	}

}
