package impl;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import api.Tree;

public class BinaryTree<K extends Comparable<K>> implements Tree<K> {
	Node<K> root;

	public BinaryTree() {
		root = null;
		
	}

	@Override
	public void insert(K data) {
		root = insertRecursion(root, data);

	}

	private Node insertRecursion(Node<K> node, K data) {
		if (node == null) {
			node = new Node<K>(data);
		} else if (data.compareTo(node.data)<0) {
			node.leftChild = insertRecursion(node.leftChild, data);
		} else if (data.compareTo(node.data)<0) {
			node.rightChild = insertRecursion(node.rightChild, data);
		}
		return node;

	}

	private Node findMinFromRight(Node node) {
		while (node.leftChild != null) {
			node = node.leftChild;
		}
		return node;
	}

	@Override
	public void delete(int data) {
		root = deleteRecursion(root, data);

	}

	private Node deleteRecursion(Node node, int data) {
		Node cur = root;
		if (cur == null) {
			return cur;
		}
		if (cur.data > data) {
			cur.leftChild = deleteRecursion(cur.leftChild, data);
		} else if (cur.data < data) {
			cur.rightChild = deleteRecursion(cur.rightChild, data);
		} else {
			if (cur.leftChild == null && cur.rightChild == null) {
				cur = null;
			} else if (cur.rightChild == null) {
				cur = cur.leftChild;
			} else if (cur.leftChild == null) {
				cur = cur.rightChild;
			} else {
				Node temp = findMinFromRight(cur.rightChild);
				cur.data = temp.data;
				cur.rightChild = deleteRecursion(cur.rightChild, temp.data);
			}
		}
		return cur;

	}

	@Override
	public boolean find(int key) {
		return findRecursion(root, key);

	}

	private boolean findRecursion(Node node, int key) {
		if (node == null) {
			return false;
		} else if (key == node.data) {
			return true;
		} else if (key > node.data) {
			return findRecursion(node.rightChild, key);
		} else {
			return findRecursion(node.leftChild, key);
		}
	}

	@Override
	public void display() {
		displayRecusrion(root);
	}

	private void displayRecusrion(Node root) {
		if (root != null) {
			displayRecusrion(root.leftChild);
			System.out.println(root.data + " ");
			displayRecusrion(root.rightChild);
		}
	}

	@Override
	public void inorder() {
		if (root != null) {
			displayRecusrion(root.leftChild);
			System.out.println(root.data + " ");
			displayRecusrion(root.rightChild);
		}

	}

	@Override
	public void postorder() {
		if (root != null) {
			displayRecusrion(root.leftChild);
			displayRecusrion(root.rightChild);
			System.out.println(root.data + " ");

		}

	}

	@Override
	public void preoredr() {
		if (root != null) {
			System.out.println(root.data + " ");
			displayRecusrion(root.leftChild);
			displayRecusrion(root.rightChild);
		}

	}

	@Override
	public int getNumberOfLeaves() {
		return getNumberOfLeaves(root);

	}

	private int getNumberOfLeaves(Node node) {
		if (node == null) {
			return 0;
		}
		if (node.leftChild == null && node.rightChild == null) {
			return 1;
		} else {
			return getNumberOfLeaves(node.leftChild)
					+ getNumberOfLeaves(node.rightChild);
		}

	}

	@Override
	public int getHeight() {
		return getHeightRecursion(root);

	}

	private int getHeightRecursion(Node node) {
		if (node == null) {
			return -1;
		}
		return 1 + Math.max(getHeightRecursion(node.leftChild),
				getHeightRecursion(node.rightChild));
	}

	@Override
	public boolean isBalanced() {
		return isBalanced(root);

	}

	// http://www.programcreek.com/2013/02/leetcode-balanced-binary-tree-java/
	private boolean isBalanced(Node root) {
		if (root == null)
			return true;

		if (getHeightRecursion(root) == -1)
			return false;

		return true;
	}

	@Override
	public void BFT() {
		BFTQueue(root);
	}

	private void BFTQueue(Node node) {
		if (node == null) {
			return;
		}
		Queue<Node> queue = new LinkedList<Node>();
		queue.add(node);
		while (!queue.isEmpty()) {
			Node temp = queue.poll();
			System.out.println(temp.data + " ");
			if (temp.leftChild != null) {
				queue.add(temp.leftChild);
			}
			if (temp.rightChild != null) {
				queue.add(temp.rightChild);
			}
		}

	}

	@Override
	public void DFT() {
		DFSStack(root);

	}

	private void DFSStack(Node root) {
		Stack<Node> s = new Stack<Node>();
		s.add(root);
		while (s.isEmpty() == false) {
			Node x = s.pop();
			if (x.rightChild != null)
				s.add(x.rightChild);
			if (x.leftChild != null)
				s.add(x.leftChild);
			System.out.print(" " + x.data);
		}
	}

	class Node<K> {
		K data;
		Node<K> leftChild;
		Node<K> rightChild;

		Node(K data) {
			this.data = data;
			leftChild = null;
			rightChild = null;
		}
	}

	class NodeLevel<K> {
		Node<K> node;
		int level;

		public NodeLevel(Node<K> node, int level) {
			super();
			this.node = node;
			this.level = level;
		}

	}

}
