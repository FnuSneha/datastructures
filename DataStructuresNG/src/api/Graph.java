package api;

import java.util.List;

public interface Graph {
	public void addEdge(int startVertex, int endVertex);

	public int getNumberOfVertices();

	public List<Integer> getNumberOfEdgesFromVertex(int startVertex);

	public void printGraph();

	public void BFT();

	public void DFT();

}
