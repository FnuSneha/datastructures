package api;

public interface Tree<K> {
	public void insert(K data);

	public void delete(int data);

	public boolean find(int key);

	public void display();

	public void inorder();

	public void postorder();

	public void preoredr();

	public int getNumberOfLeaves();

	public int getHeight();

	public boolean isBalanced();

	public void BFT();

	public void DFT();

}
