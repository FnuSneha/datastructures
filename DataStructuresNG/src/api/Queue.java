package api;

import api.exception.QueueEmptyException;

public interface Queue {
	public void enqueue(Object obj);
	public Object dequeue() throws QueueEmptyException;
	public int size();
	public boolean isEmpty();
}
