package api;

public interface Heap {
	public void insert(int data);

	public void print();

	public int remove();

	public boolean isLeaf(int pos);

	public int getLeftChild(int pos);

	public int getRightChild(int pos);

	public int getParent(int pos);

	public int getMin();

}
