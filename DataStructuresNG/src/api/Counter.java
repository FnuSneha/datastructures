package api;

public interface Counter {
	public void increment();

	public int value();
}
