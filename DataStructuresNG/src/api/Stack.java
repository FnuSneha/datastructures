package api;

import api.exception.StackEmptyException;

public interface Stack {
	public void push(Object obj);
	public Object pop() throws StackEmptyException;
	public boolean isEmpty();
	public Object top() throws StackEmptyException;
	public int size();

}
